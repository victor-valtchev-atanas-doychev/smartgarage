package com.example.smartgarage;

import com.example.smartgarage.models.*;
import com.example.smartgarage.models.filterParameters.CustomersSortParameters;
import com.example.smartgarage.models.filterParameters.ServiceOrderFilterParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

public class Helpers {

    public static Brand createMockBrand(){
        Brand mockBrand = new Brand();
        mockBrand.setId(1);
        mockBrand.setBrandName("Mercedes-Benz");
        return mockBrand;
    }

    public static Model createMockModel(){
        Model mockModel = new Model();
        mockModel.setId(1);
        mockModel.setModelName("S-class 600");
        mockModel.setBrand(createMockBrand());
        return mockModel;
    }

    public static Vehicle createMockVehicle(){
        Vehicle mockVehicle = new Vehicle();
        mockVehicle.setId(1);
        mockVehicle.setLicensePlate("B0717PP");
        mockVehicle.setVin("00000000000000017");
        mockVehicle.setYear(2020);
        mockVehicle.setOwner(createMockCustomer());
        mockVehicle.setModel(createMockModel());
        return mockVehicle;
    }

    public static ShopService createMockShopService(){
        ShopService mockShopService = new ShopService();
        mockShopService.setId(1);
        mockShopService.setName("Oil Change");
        mockShopService.setPrice(50);
        return mockShopService;
    }

    public static Visit createMockVisit(){
        Visit mockVisit = new Visit();
        mockVisit.setId(1);
        mockVisit.setVehicle(createMockVehicle());
        mockVisit.setStartDate(LocalDateTime.of(2021, 10, 10, 0, 0, 0, 0));
        mockVisit.setStartDate(LocalDateTime.of(2021, 10, 15, 0, 0, 0, 0));
        return mockVisit;
    }

    public static ServiceOrder createMockServiceOrder(){
        ServiceOrder mockServiceOrder = new ServiceOrder();
        mockServiceOrder.setId(1);
        mockServiceOrder.setService(createMockShopService());
        mockServiceOrder.setVisit(createMockVisit());
        return mockServiceOrder;
    }

    public static User createMockUser() {
        User mockUser = new User();
        mockUser.setId(7);
        mockUser.setFirstName("Petq");
        mockUser.setLastName("Dobarova");
        mockUser.setPhone("123456789");
        mockUser.setEmail("mock@mailmock.com");
        mockUser.setRoles(null);
        return mockUser;
    }

    public static User createMockCustomer() {
        User mockCustomer = new User();
        mockCustomer.setId(1);
        mockCustomer.setFirstName("Victor");
        mockCustomer.setLastName("Valtchev");
        mockCustomer.setPhone("987654321");
        mockCustomer.setEmail("v.v@gmail.com");
        mockCustomer.setRoles(Set.of(createMockRoleCustomer()));
        return mockCustomer;
    }

    public static User createMockEmployee() {
        User mockEmployee = new User();
        mockEmployee.setId(2);
        mockEmployee.setFirstName("Atanas");
        mockEmployee.setLastName("Doychev");
        mockEmployee.setPhone("6666666666");
        mockEmployee.setEmail("d.d@gmail.com");
        mockEmployee.setRoles(Set.of(createMockRoleEmployee()));
        return mockEmployee;
    }

    public static User createMockAdmin() {
        User mockEmployee = new User();
        mockEmployee.setId(3);
        mockEmployee.setFirstName("Admin");
        mockEmployee.setLastName("Admin");
        mockEmployee.setPhone("1234567890");
        mockEmployee.setEmail("admin@gmail.com");
        mockEmployee.setRoles(Set.of(createMockRoleAdmin()));
        return mockEmployee;
    }

    public static Role createMockRoleCustomer() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRole("Customer");
        return mockRole;
    }

    public static Role createMockRoleEmployee() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRole("Employee");
        return mockRole;
    }

    public static Role createMockRoleAdmin() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRole("Admin");
        return mockRole;
    }

    public static CustomersSortParameters createMockCSP(){

        CustomersSortParameters csp = new CustomersSortParameters();
        Optional<String> acs = Optional.of("asc");
        csp.setDateOrder(acs);
        return csp;
    }

    public static UserFilterParameters createMockUFP(){
        UserFilterParameters mockUFP = new UserFilterParameters();
        Optional<String> name = Optional.of("name");
        mockUFP.setFirstName(name);
        return mockUFP;
    }

    public static UserInfoUpdateDto createMockUserInfoUpdateDto() {

        UserInfoUpdateDto mockUiuDto = new UserInfoUpdateDto();

        mockUiuDto.setFirstName("test1");
        mockUiuDto.setLastName("test2");
        mockUiuDto.setPhone("098765123");
        mockUiuDto.setPassword("mockPass");

        return mockUiuDto;
    }

    public static ServiceOrderFilterParameters createMockServiceOrderFilterParameters(){

        ServiceOrderFilterParameters mockSOFP = new ServiceOrderFilterParameters();
        return mockSOFP;
    }

    public static ConfirmationToken createMockConfirmationToken() {

        ConfirmationToken mockToken = new ConfirmationToken();

        long id = 1;
        mockToken.setId(id);
        mockToken.setToken("023d3dd9-f309-41b4-b955-f33c4526bb9f");
        mockToken.setCreatedAt(LocalDateTime.now().minusHours(1));
        mockToken.setExpiresAt(LocalDateTime.now().plusHours(1));
        mockToken.setConfirmedAt(LocalDateTime.now());
        mockToken.setUser(createMockCustomer());

        return mockToken;
    }

    public static UserRoles createMockUserRoles() {
        UserRoles mockUserRoles = new UserRoles();

        long id = 1;
        mockUserRoles.setId(id);
        mockUserRoles.setUser(createMockEmployee());
        mockUserRoles.setRole(createMockRoleCustomer());

        return mockUserRoles;
    }
}
