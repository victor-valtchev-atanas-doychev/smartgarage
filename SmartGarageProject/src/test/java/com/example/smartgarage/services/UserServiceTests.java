package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.DuplicateEntityException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserInfoUpdateDto;
import com.example.smartgarage.models.filterParameters.CustomersSortParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;
import com.example.smartgarage.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.smartgarage.Helpers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl mockUserService;


    @Test
    public void getAll_Should_CallUserRepository() {
        User mockUser = createMockEmployee();
        mockUserService.getAll(mockUser);
        Mockito.verify(mockUserRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAllCustomersWithSort_Should_CallUserRepository() {
        User mockUser = createMockEmployee();
        CustomersSortParameters mockCSP = createMockCSP();
        mockUserService.getAllCustomersWithSort(mockUser, mockCSP);
        Mockito.verify(mockUserRepository, Mockito.times(
                1)).getAllCustomersWithSort(mockCSP);
    }

    @Test
    public void getById_Should_CallUserRepository() {
        User mockUser = createMockEmployee();

        mockUserService.getById(1, mockUser);

        Mockito.verify(mockUserRepository, Mockito.times(
                1)).getById(1);
    }

    @Test
    public void getUserByEmail_Should_CallUserRepository() {
        String mockMail = "test@test.com";

        mockUserService.getUserByEmail(mockMail);

        Mockito.verify(mockUserRepository, Mockito.times(
                1)).getUserByEmail(mockMail);
    }

    @Test
    public void filter_Should_CallUserRepository() {
        UserFilterParameters mockUFP = createMockUFP();
        User mockUser = createMockEmployee();

        mockUserService.filter(mockUser, mockUFP);

        Mockito.verify(mockUserRepository, Mockito.times(
                1)).filter(mockUFP);
    }

    @Test
    public void create_Should_CallUserRepository() {
        User mockUser = createMockUser();
        User mockEmployee = createMockEmployee();

        mockUserService.create(mockUser, mockEmployee);

        Mockito.verify(mockUserRepository, Mockito.times(
                1)).create(mockUser);
    }

    @Test
    public void update_Should_CallUserRepository() {
        User mockUser = createMockEmployee();
        User mockEmployee = createMockEmployee();

//        mockUserService.checkIfDuplicateUserExists(mockUser);
//
//        doThrow(DuplicateEntityException.class)
//                .when(mockUserService)
//                .checkIfDuplicateUserExists(mockUser);

        mockUserService.update(mockUser, mockEmployee);

        Mockito.verify(mockUserRepository, Mockito.times(
                1)).update(mockUser);
    }

    @Test
    public void updateByUser_Should_CallUserRepository(){
        User mockUser = createMockEmployee();
        User mockDto = createMockUser();

        mockUserService.updateByUser(mockUser, mockDto);

        Mockito.verify(mockUserRepository, Mockito.times(
                1)).update(mockUser);
    }

    @Test
    public void delete_Should_CallUserRepository(){
        User mockUser = createMockEmployee();
        User mockUser2 = createMockUser();

        mockUserService.delete(1,mockUser);

        Mockito.verify(mockUserRepository, Mockito.times(
                1)).delete(mockUser2);
    }

}
