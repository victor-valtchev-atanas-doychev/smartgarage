package com.example.smartgarage.services;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.createMockEmployee;

@ExtendWith(MockitoExtension.class)

public class VisitServiceTests {

    @Mock
    VisitRepository visitRepository;

    @InjectMocks
    VisitServiceImpl visitService;


    @Test
    public void getAll_Should_CallRepository() {
        User mockUser = createMockEmployee();
        visitService.getAll(mockUser);
        Mockito.verify(visitRepository, Mockito.times(
                1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Visit mockVisit = new Visit();
        visitService.getById(1, mockUser);
        Mockito.verify(visitRepository, Mockito.times(
                1)).getById(1);
    }

    @Test
    public void create_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Visit mockVisit = new Visit();
        visitService.create(mockVisit, mockUser);
        Mockito.verify(visitRepository, Mockito.times(
                1)).create(mockVisit);
    }

    @Test
    public void update_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Visit mockVisit = new Visit();
        visitService.update(mockVisit, mockUser);
        Mockito.verify(visitRepository, Mockito.times(
                1)).update(mockVisit);
    }

    @Test
    public void delete_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Visit mockVisit = new Visit();
        visitService.delete(1, mockUser);
        Mockito.verify(visitRepository, Mockito.times(
                1)).delete(mockVisit);
    }

    @Test
    public void getAllVisitsByVehicleId_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Visit mockVisit = new Visit();
        visitService.getAllVisitsByVehicleId("1", mockUser);
        Mockito.verify(visitRepository, Mockito.times(
                1)).getAllVisitsByVehicleId("1");
    }
}
