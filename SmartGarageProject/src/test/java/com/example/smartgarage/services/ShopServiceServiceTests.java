package com.example.smartgarage.services;

import com.example.smartgarage.models.ShopService;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ShopServiceFilterParameters;
import com.example.smartgarage.repositories.contracts.ShopServiceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.createMockEmployee;
import static com.example.smartgarage.Helpers.createMockShopService;

@ExtendWith(MockitoExtension.class)
public class ShopServiceServiceTests {

    @Mock
    ShopServiceRepository shopServiceRepository;

    @InjectMocks
    ShopServiceServiceImpl shopServiceService;


    @Test
    public void getAll_Should_CallRepository() {
        User mockUser = createMockEmployee();
        shopServiceService.getAll(mockUser);
        Mockito.verify(shopServiceRepository, Mockito.times(
                1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        User mockUser = createMockEmployee();
        shopServiceService.getById(1, mockUser);
        Mockito.verify(shopServiceRepository, Mockito.times(
                1)).getById(1);
    }

    @Test
    public void create_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ShopService mockShopService = createMockShopService();
        shopServiceService.create(mockShopService, mockUser);
        Mockito.verify(shopServiceRepository, Mockito.times(
                1)).create(mockShopService);
    }

    @Test
    public void update_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ShopService mockShopService = createMockShopService();
        shopServiceService.update(mockShopService, mockUser);
        Mockito.verify(shopServiceRepository, Mockito.times(
                1)).update(mockShopService);
    }

    @Test
    public void delete_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ShopService mockShopService = createMockShopService();
        shopServiceService.delete(1, mockUser);
        Mockito.verify(shopServiceRepository, Mockito.times(
                1)).delete(mockShopService);
    }

    @Test
    public void getShopServicesByNameAndPrice_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ShopServiceFilterParameters mockSSFP = new ShopServiceFilterParameters();
        shopServiceService.getShopServicesByNameAndPrice(mockSSFP, mockUser);
        Mockito.verify(shopServiceRepository, Mockito.times(
                1)).getShopServicesByNameAndPrice(mockSSFP);
    }

    @Test
    public void getAllServicesLinkedToACustomer_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ShopServiceFilterParameters mockSSFP = new ShopServiceFilterParameters();
        shopServiceService.getAllServicesLinkedToACustomer(mockUser, mockSSFP);
        Mockito.verify(shopServiceRepository, Mockito.times(
                1)).getAllServicesLinkedToACustomer(mockUser, mockSSFP);
    }

}
