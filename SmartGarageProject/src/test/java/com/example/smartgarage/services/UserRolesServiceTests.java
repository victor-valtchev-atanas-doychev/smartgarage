package com.example.smartgarage.services;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserRoles;
import com.example.smartgarage.repositories.contracts.UserRolesRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)

public class UserRolesServiceTests {

    @Mock
    UserRolesRepository userRolesRepository;

    @InjectMocks
    UserRolesServiceImpl userRolesService;


    @Test
    public void getAll_Should_CallRepository() {
        User mockUser = createMockAdmin();
        userRolesService.getAll(mockUser);
        Mockito.verify(userRolesRepository, Mockito.times(
                1)).getAll();
    }

    @Test
    public void addRole_Should_CallRepository() {
        User mockUser = createMockAdmin();
        UserRoles mockUserRoles = createMockUserRoles();
        userRolesService.addRole(mockUser, mockUserRoles);
        Mockito.verify(userRolesRepository, Mockito.times(
                1)).addRole(mockUserRoles);
    }

    @Test
    public void deleteRole_Should_CallRepository() {
        User mockUser = createMockAdmin();
        UserRoles mockUserRoles = createMockUserRoles();
        userRolesService.deleteRole(mockUser, mockUserRoles);

        UserRoles userRolesToDelete = userRolesRepository.
                getByUserAndRole(mockUserRoles);

        Mockito.verify(userRolesRepository, Mockito.times(
                1)).deleteRole(userRolesToDelete);
    }
}
