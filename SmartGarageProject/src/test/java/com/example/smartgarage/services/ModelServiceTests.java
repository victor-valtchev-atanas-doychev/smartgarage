package com.example.smartgarage.services;

import com.example.smartgarage.models.Model;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.createMockEmployee;
import static com.example.smartgarage.Helpers.createMockModel;

@ExtendWith(MockitoExtension.class)

public class ModelServiceTests {

    @Mock
    ModelRepository modelRepository;

    @InjectMocks
    ModelServiceImpl modelService;


    @Test
    public void getAll_Should_CallRepository() {
        User mockUser = createMockEmployee();
        modelService.getAll(mockUser);
        Mockito.verify(modelRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        User mockUser = createMockEmployee();
        modelService.getById(1, mockUser);
        Mockito.verify(modelRepository,
                Mockito.times(1)).getById(1);
    }

    @Test
    public void create_Should_CallRepository() {
        Model mockModel = createMockModel();
        User mockUser = createMockEmployee();
        modelService.create(mockModel, mockUser);
        Mockito.verify(modelRepository,
                Mockito.times(1)).create(mockModel);
    }

    @Test
    public void update_Should_CallRepository() {
        Model mockModel = createMockModel();
        User mockUser = createMockEmployee();
        modelService.update(mockModel, mockUser);
        Mockito.verify(modelRepository,
                Mockito.times(1)).update(mockModel);
    }

    @Test
    public void delete_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Model mockModel = createMockModel();
        modelService.delete(1, mockUser);
        mockModel.setId(1);
        Mockito.verify(modelRepository,
                Mockito.times(1)).delete(mockModel);
    }
}
