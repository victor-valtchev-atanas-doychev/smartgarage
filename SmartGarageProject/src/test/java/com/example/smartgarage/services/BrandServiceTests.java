package com.example.smartgarage.services;

import com.example.smartgarage.models.Brand;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.BrandRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.createMockBrand;
import static com.example.smartgarage.Helpers.createMockEmployee;

@ExtendWith(MockitoExtension.class)
public class BrandServiceTests {

    @Mock
    BrandRepository mockBrandRepository;

    @InjectMocks
    BrandServiceImpl mockBrandService;


    @Test
    public void getAll_Should_CallRepository() {
        User mockUser = createMockEmployee();
        mockBrandService.getAll(mockUser);
        Mockito.verify(mockBrandRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        User mockUser = createMockEmployee();
        mockBrandService.getById(1, mockUser);
        Mockito.verify(mockBrandRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void create_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Brand mockBrand = createMockBrand();
        mockBrandService.create(mockBrand, mockUser);
        Mockito.verify(mockBrandRepository, Mockito.times(1)).create(mockBrand);
    }

    @Test
    public void update_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Brand mockBrand = createMockBrand();
        mockBrandService.update(mockBrand, mockUser);
        Mockito.verify(mockBrandRepository, Mockito.times(1)).update(mockBrand);
    }

    @Test
    public void delete_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Brand mockBrand = createMockBrand();
        mockBrandService.delete(1, mockUser);
        Mockito.verify(mockBrandRepository, Mockito.times(1)).delete(mockBrand);
    }
}
