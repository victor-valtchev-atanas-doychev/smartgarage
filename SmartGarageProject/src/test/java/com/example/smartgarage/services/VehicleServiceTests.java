package com.example.smartgarage.services;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.repositories.contracts.VehicleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static com.example.smartgarage.Helpers.createMockEmployee;
import static com.example.smartgarage.Helpers.createMockVehicle;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceTests {

    @Mock
    VehicleRepository vehicleRepository;

    @InjectMocks
    VehicleServiceImpl vehicleService;


    @Test
    public void getAll_Should_CallRepository() {
        User mockUser = createMockEmployee();
        vehicleService.getAll(mockUser);
        Mockito.verify(vehicleRepository, Mockito.times(
                1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Vehicle mockVehicle = createMockVehicle();
        vehicleService.getById(1, mockUser);
        Mockito.verify(vehicleRepository, Mockito.times(
                1)).getById(1);
    }

    @Test
    public void create_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Vehicle mockVehicle = createMockVehicle();
        vehicleService.create(mockVehicle, mockUser);
        Mockito.verify(vehicleRepository, Mockito.times(
                1)).create(mockVehicle);
    }

    @Test
    public void update_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Vehicle mockVehicle = createMockVehicle();
        vehicleService.update(mockVehicle, mockUser);
        Mockito.verify(vehicleRepository, Mockito.times(
                1)).update(mockVehicle);
    }

    @Test
    public void delete_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Vehicle mockVehicle = createMockVehicle();
        vehicleService.delete(1, mockUser);
        Mockito.verify(vehicleRepository, Mockito.times(
                1)).delete(mockVehicle);
    }

    @Test
    public void getByOwner_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Vehicle mockVehicle = createMockVehicle();
        vehicleService.getByOwner(1, mockUser);
        Mockito.verify(vehicleRepository, Mockito.times(
                1)).getByOwnerId(1);
    }

    @Test
    public void getAllVehiclesByMonth_Should_CallRepository() {
        User mockUser = createMockEmployee();
        Vehicle mockVehicle = createMockVehicle();
        vehicleService.getAllVehiclesByMonth("2000", "April", mockUser);
        Mockito.verify(vehicleRepository, Mockito.times(
                1)).getAllVehiclesByMonth(
                        LocalDateTime.now(), LocalDateTime.now().plusMonths(1));
    }
}
