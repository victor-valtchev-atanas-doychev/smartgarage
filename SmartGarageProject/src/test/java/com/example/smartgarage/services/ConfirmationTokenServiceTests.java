package com.example.smartgarage.services;

import com.example.smartgarage.models.ConfirmationToken;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.ConfirmationTokenRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.createMockConfirmationToken;
import static com.example.smartgarage.Helpers.createMockEmployee;

@ExtendWith(MockitoExtension.class)
public class ConfirmationTokenServiceTests {

    @Mock
    ConfirmationTokenRepository confirmationTokenRepository;

    @InjectMocks
    ConfirmationTokenServiceImpl confirmationTokenService;


    @Test
    public void create_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ConfirmationToken mockToken = createMockConfirmationToken();

        confirmationTokenService.create(mockUser, 1);

        Mockito.verify(confirmationTokenRepository,
                Mockito.times(1)).create(mockToken);
    }

    @Test
    public void getToken_Should_CallRepository() {
        String mockToken = ("023d3dd9-f309-41b4-b955-f33c4526bb9f");

        confirmationTokenService.getToken(mockToken);

        Mockito.verify(confirmationTokenRepository,
                Mockito.times(1)).getToken(mockToken);
    }

    @Test
    public void setConfirmedAt_Should_CallRepository() {
        ConfirmationToken mockToken = createMockConfirmationToken();

        confirmationTokenService.setConfirmedAt(mockToken);

        Mockito.verify(confirmationTokenRepository,
                Mockito.times(1)).setConfirmedAt(mockToken);
    }
}
