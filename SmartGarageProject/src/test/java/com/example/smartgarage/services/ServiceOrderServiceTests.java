package com.example.smartgarage.services;

import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ServiceOrderFilterParameters;
import com.example.smartgarage.repositories.contracts.ServiceOrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ServiceOrderServiceTests {

    @Mock
    ServiceOrderRepository serviceOrderRepository;

    @InjectMocks
    ServiceOrderServiceImpl serviceOrderService;


    @Test
    public void getAll_Should_CallRepository() {
        User mockUser = createMockEmployee();
        serviceOrderService.getAll(mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        User mockUser = createMockEmployee();
        serviceOrderService.getById(1, mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).getById(1);
    }

    @Test
    public void create_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ServiceOrder mockServiceOrder = createMockServiceOrder();
        serviceOrderService.create(mockServiceOrder, mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).create(mockServiceOrder);
    }

    @Test
    public void update_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ServiceOrder mockServiceOrder = createMockServiceOrder();
        serviceOrderService.update(mockServiceOrder, mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).update(mockServiceOrder);
    }

    @Test
    public void delete_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ServiceOrder mockServiceOrder = createMockServiceOrder();
        serviceOrderService.delete(1, mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).delete(mockServiceOrder);
    }

    @Test
    public void getAllServicesForMonth_Should_CallRepository() {
        User mockUser = createMockEmployee();
        serviceOrderService.getAllServicesForMonth("1999", "March", mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1))
                .getAllServicesForMonth(LocalDateTime.now(), LocalDateTime.now());
    }

    @Test
    public void getAllServicesByVisitId_Should_CallRepository() {
        User mockUser = createMockEmployee();
        serviceOrderService.getAllServicesByVisitId(1, mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).getAllServicesByVisitId(1);
    }

    @Test
    public void createFromFrontEnd_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ServiceOrder mockServiceOrder = createMockServiceOrder();
        serviceOrderService.createFromFrontEnd(mockServiceOrder, mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).create(mockServiceOrder);
    }

    @Test
    public void getAllServicesByUserId_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ServiceOrder mockServiceOrder = createMockServiceOrder();
        serviceOrderService.getAllServicesByUserId(1, mockUser);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).getAllServicesByUserId(1);
    }

    @Test
    public void filterByVehicleIdAndOrStartDate_Should_CallRepository() {
        User mockUser = createMockEmployee();
        ServiceOrderFilterParameters mockSOFP = createMockServiceOrderFilterParameters();
        serviceOrderService.filterByVehicleIdAndOrStartDate(mockUser, mockSOFP);
        Mockito.verify(serviceOrderRepository,
                Mockito.times(1)).filterByVehicleIdAndOrStartDate(mockSOFP);
    }
}
