package com.example.smartgarage.services;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.repositories.contracts.VehicleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceImplTests {

    @Mock
    VehicleRepository mockVehicleRepository;

    @InjectMocks
    VehicleServiceImpl mockVehicleService;

    @Test
    public void getAll_Should_CallService() {
        User mockEmployee = createMockEmployee();
        mockVehicleService.getAll(mockEmployee);
        Mockito.verify(mockVehicleRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallService() {
        User mockEmployee = createMockEmployee();
        mockVehicleService.getById(1, mockEmployee);
        Mockito.verify(mockVehicleRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void create_Should_CallService(){
        User mockEmployee = createMockEmployee();
        Vehicle mockVehicle = createMockVehicle();
        mockVehicleService.create(mockVehicle, mockEmployee);
        Mockito.verify(mockVehicleRepository, Mockito.times(1)).create(mockVehicle);
    }
}
