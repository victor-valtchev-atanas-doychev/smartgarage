package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.exceptions.AuthenticationFailureException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.exceptions.OrderOfOperationsException;
import com.example.smartgarage.exceptions.UnauthorizedOperationException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.mappers.UserMapper;
import com.example.smartgarage.services.contracts.RestartPasswordService;
import com.example.smartgarage.services.contracts.UserService;
import com.example.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/my-profile")
public class MyProfileMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final RestartPasswordService restartPasswordService;
    private final UserService userService;
    private final VisitService visitService;
    private final UserMapper userMapper;

    @Autowired
    public MyProfileMvcController(AuthenticationHelper authenticationHelper,
                                  RestartPasswordService restartPasswordService,
                                  UserService userService, VisitService visitService,
                                  UserMapper userMapper) {
        this.authenticationHelper = authenticationHelper;
        this.restartPasswordService = restartPasswordService;
        this.userService = userService;
        this.visitService = visitService;
        this.userMapper = userMapper;
    }


    @GetMapping
    public String showUserPage(Model model, HttpSession session) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }

        model.addAttribute("passChangeFromUserDto", new PassChangeFromUserDto());
        model.addAttribute("userInfoUpdateDto", new UserInfoUpdateDto());
        model.addAttribute("visitIdDto", new VisitIdDto());

        return "my-profile";
    }


    //here - Users can change their password while logged in
    @PostMapping("/user-pass-change")
    public String handleUserPassChange(@Valid @ModelAttribute("passChangeFromUserDto")
                                               PassChangeFromUserDto passChangeFromUserDto,
                                       BindingResult bindingResult, HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }

        User userToConfirmAndChange = authenticationHelper.tryGetUser(session);

        try {
            restartPasswordService.changeUserPassword(userToConfirmAndChange, passChangeFromUserDto);

        } catch (AuthenticationFailureException e) {

            bindingResult.rejectValue("oldPassword", "auth_error", e.getMessage());
            return "redirect:/my-profile";

        } catch (OrderOfOperationsException e) {

            bindingResult.rejectValue("repeatNewPassword", "auth_error", e.getMessage());
            return "redirect:/my-profile";

        } catch (UnauthorizedOperationException e) {

            return "redirect:/notFound";

        }

        return "redirect:/my-profile";
    }

    //here - Users can update info
    @PostMapping("/update-user-info")
    public String handleUserInfoUpdate(@Valid @ModelAttribute("userInfoUpdateDto")
                                               UserInfoUpdateDto userInfoUpdateDto,
                                       BindingResult bindingResult, HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }

        try {
            User userToVerify = authenticationHelper.tryGetUser(session);
            User userToUpdate = userMapper.fromUserInfoUpdateDto(userInfoUpdateDto);

            userService.updateByUser(userToVerify, userToUpdate);

        } catch (AuthenticationFailureException e) {

            bindingResult.rejectValue("firstName", "auth_error", e.getMessage());
            return "redirect:/my-profile";

        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }
        return "redirect:/my-profile";
    }

    //here - Clients send mail with all service orders for a visit to their email
    @PostMapping("/send/all-orders-for-visit")
    public String handleAllOrdersOfAVisit(@Valid @ModelAttribute("visitIdDto")
                                          VisitIdDto visitIdDto,
                                          BindingResult bindingResult,
                                          HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/my-profile";
        }

        try {

            User userToVerify = authenticationHelper.tryGetUser(session);

            visitService.sendMailOfAllOrdersForAVisit(visitIdDto.getVisitId(), userToVerify);

        } catch (UnauthorizedOperationException |
                AuthenticationFailureException |
                EntityNotFoundException e) {
            bindingResult.rejectValue("visitId", "auth_error", e.getMessage());
            return "redirect:/my-profile";
        }

        return "redirect:/my-profile";
    }
}
