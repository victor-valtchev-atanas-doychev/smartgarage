package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitDto;
import com.example.smartgarage.models.mappers.VisitMapper;
import com.example.smartgarage.repositories.util.RepositoryHelper;
import com.example.smartgarage.services.contracts.VisitService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/visits")
public class VisitController {

    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final AuthenticationHelper authenticationHelper;

    public VisitController(VisitService visitService,
                           VisitMapper visitMapper,
                           AuthenticationHelper authenticationHelper) {

        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Visit createVisit(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody VisitDto visitDto) {

        User user = authenticationHelper.tryGetUser(headers);
        Visit visit = visitMapper.fromDtoToObjectWhenCreateOrUpdate(visitDto);
        visitService.create(visit, user);
        return visit;
    }
    @PostMapping(value = "/front-end", consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Visit createVisitFrontEnd(@RequestHeader HttpHeaders headers,
                             VisitDto visitDto) {

        User user = authenticationHelper.tryGetUser(headers);
        Visit visit = visitMapper.fromDtoToObjectWhenCreateOrUpdate(visitDto);
        visitService.create(visit, user);
        return visit;
    }

    @GetMapping
    public List<Visit> getAllVisitsOrById(@RequestHeader HttpHeaders headers,
                                          @RequestParam(name = "id", required = false)
                                                  Optional<Integer> id) {
        User user = authenticationHelper.tryGetUser(headers);
        if (id.isPresent()) {
            List<Visit> list = new ArrayList<>();
            list.add(visitService.getById(id.get(), user));
            return list;
        } else {
            return visitService.getAll(user);
        }
    }

    @GetMapping("/date")
    public List<Visit> getVisitByStartAndEndDate(@RequestHeader HttpHeaders headers,
                                                 @RequestParam(name = "startDate", required = false) Optional<String> startDate,
                                                 @RequestParam(name = "endDate", required = false) Optional<String> endDate) {
        User user = authenticationHelper.tryGetUser(headers);
        return visitService.getVisitByStartAndEndDate(startDate, endDate, user);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateVisit(@RequestHeader HttpHeaders headers,
                            @Valid @RequestBody VisitDto visitDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Visit visit = visitMapper.fromDtoToObjectWhenCreateOrUpdate(visitDto);
        visitService.update(visit, user);
    }

    @PutMapping(value = "/front-end", consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateVisitFrontEnd(@RequestHeader HttpHeaders headers,
                            VisitDto visitDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Visit visit = visitMapper.fromDtoToObjectWhenCreateOrUpdate(visitDto);
        visitService.update(visit, user);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteVisit(@RequestHeader HttpHeaders headers,
                            @RequestParam(name = "id") int id) {
        User user = authenticationHelper.tryGetUser(headers);
        visitService.delete(id, user);
    }

    @GetMapping("/getbymonth")
    public List<Visit> getAllVisitsByMonth(@RequestHeader HttpHeaders headers,
                                           @RequestParam(name = "year") String year,
                                           @RequestParam(name = "month") String month) {
        User user = authenticationHelper.tryGetUser(headers);
        return visitService.getAllVisitsByMonth(year, month, user);
    }

    @GetMapping("/getbyvehicleid")
    public List<Visit> getAllVisitsByVehicleId(@RequestHeader HttpHeaders headers,
                                               @RequestParam(name = "vehicleId") String vehicleId){
        User user = authenticationHelper.tryGetUser(headers);
        return visitService.getAllVisitsByVehicleId(vehicleId, user);
    }
}