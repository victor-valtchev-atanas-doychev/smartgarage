package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.exceptions.AuthenticationFailureException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.exceptions.OrderOfOperationsException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.mappers.PassRestartRequestMapper;
import com.example.smartgarage.services.contracts.ConfirmationTokenService;
import com.example.smartgarage.services.contracts.RestartPasswordService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;

@Controller
@RequestMapping
public class PassResetMvcController {

    private final RestartPasswordService restartPasswordService;
    private final ConfirmationTokenService confirmationTokenService;
    private final AuthenticationHelper authenticationHelper;
    private final PassRestartRequestMapper passRestartRequestMapper;

    public PassResetMvcController(RestartPasswordService restartPasswordService,
                                  ConfirmationTokenService confirmationTokenService,
                                  PassRestartRequestMapper passRestartRequestMapper,
                                  AuthenticationHelper authenticationHelper) {
        this.restartPasswordService = restartPasswordService;
        this.confirmationTokenService = confirmationTokenService;
        this.authenticationHelper = authenticationHelper;
        this.passRestartRequestMapper = passRestartRequestMapper;
    }

    //here - Users/customers can request password restart
    @GetMapping("/request-pass-restart")
    public String showPassRestartRequestPage(Model model) {
        model.addAttribute("passRestartRequestDto", new PassRestartRequestDto());
        return "request-pass-restart";
    }

    @PostMapping("/request-pass-restart")
    public String handlePassRestartRequest(@Valid @ModelAttribute("passRestartRequestDto")
                                           PassRestartRequestDto passRestartRequestDto,
                                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "request-pass-restart";
        }

        try {
            PassRestartRequest passRestartRequest = passRestartRequestMapper.fromDto(passRestartRequestDto);
            restartPasswordService.sendResetPasswordMailAutomatically(passRestartRequestDto.getEmail());
            restartPasswordService.createRestartPasswordRequest(passRestartRequest);

        } catch (AuthenticationFailureException | EntityNotFoundException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return "request-pass-restart";
        }

        return "redirect:/";
    }


    //here - Users/customers can restart their password from link received in their email
    @GetMapping("/user-pass-restart")
    public String showPassRestartPage(@RequestParam(name = "token") String token,
                                      Model model, HttpSession session) {

        session.setAttribute("passRestartToken", token);
        model.addAttribute("passRestartFromUserDto", new PassRestartFromUserDto());

        ConfirmationToken tokenToDeactivate = confirmationTokenService.getToken(token);
        if (tokenToDeactivate.isUsed()){
            return "notFound";
        }
        if (tokenToDeactivate.getExpiresAt().isBefore(LocalDateTime.now())){
            return "notFound";
        }
        confirmationTokenService.setConfirmedAt(tokenToDeactivate);
        return "user-pass-restart";
    }

    @PostMapping("/user-pass-restart")
    public String handlePassRestart(@Valid @ModelAttribute("passRestartFromUserDto")
                                    PassRestartFromUserDto passRestartFromUserDto,
                                    BindingResult bindingResult,
                                    HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "user-pass-restart";
        }

        String token = (String) session.getAttribute("passRestartToken");

        if (!passRestartFromUserDto.getPassword().equals(passRestartFromUserDto.getRepeatPassword())) {
            bindingResult.rejectValue("repeatPassword", "password_error", "Passwords don't match!");
        }

        try{
            ConfirmationToken confirmationToken = confirmationTokenService.getToken(token);
            restartPasswordService.resetUserPassword(confirmationToken, passRestartFromUserDto);

        } catch (OrderOfOperationsException | EntityNotFoundException e) {
            bindingResult.rejectValue("password", "password_error", e.getMessage());
            return "user-pass-restart";
        } catch (IllegalStateException e) {
            bindingResult.rejectValue("repeatPassword", "password_error", e.getMessage());
            return "user-pass-restart";
        }

        session.removeAttribute("passRestartToken");

        return "redirect:/login";
    }
}
