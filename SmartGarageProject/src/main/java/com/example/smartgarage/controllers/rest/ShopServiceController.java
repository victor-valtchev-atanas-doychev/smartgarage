package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.exceptions.OrderOfOperationsException;
import com.example.smartgarage.models.ShopService;
import com.example.smartgarage.models.ShopServiceDto;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ShopServiceFilterParameters;
import com.example.smartgarage.models.mappers.ShopServiceMapper;
import com.example.smartgarage.services.contracts.ShopServiceService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/shopservices")
public class ShopServiceController {

    private final ShopServiceService shopServiceService;
    private final ShopServiceMapper shopServiceMapper;
    private final AuthenticationHelper authenticationHelper;

    public ShopServiceController(ShopServiceService shopServiceService,
                                 ShopServiceMapper shopServiceMapper,
                                 AuthenticationHelper authenticationHelper) {

        this.shopServiceService = shopServiceService;
        this.shopServiceMapper = shopServiceMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public ShopService createShopService(@RequestHeader HttpHeaders headers,
                                         @Valid @RequestBody ShopServiceDto shopServiceDto) {

        User user = authenticationHelper.tryGetUser(headers);
        ShopService shopService = shopServiceMapper.fromDtoToObjectWhenCreateOrUpdate(shopServiceDto);
        shopServiceService.create(shopService, user);
        return shopService;
    }

    @PostMapping(value = "/front-end", consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ShopService createShopServiceFrontEnd(@RequestHeader HttpHeaders headers,
                                         ShopServiceDto shopServiceDto) {

        User user = authenticationHelper.tryGetUser(headers);
        ShopService shopService = shopServiceMapper.fromDtoToObjectWhenCreateOrUpdate(shopServiceDto);
        shopServiceService.create(shopService, user);
        return shopService;
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateShopService(@RequestHeader HttpHeaders headers,
                                  @Valid @RequestBody ShopServiceDto shopServiceDto) {
        User user = authenticationHelper.tryGetUser(headers);
        ShopService shopService = shopServiceMapper.fromDtoToObjectWhenCreateOrUpdate(shopServiceDto);
        shopServiceService.update(shopService, user);
    }

    @PutMapping(value = "/front-end", consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateShopServiceFrontEnd(@RequestHeader HttpHeaders headers,
                                  ShopServiceDto shopServiceDto) {
        User user = authenticationHelper.tryGetUser(headers);
        ShopService shopService = shopServiceMapper.fromDtoToObjectWhenCreateOrUpdate(shopServiceDto);
        shopServiceService.update(shopService, user);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteShopService(@RequestHeader HttpHeaders headers,
                                  @RequestParam(name = "id") int id) {
        User user = authenticationHelper.tryGetUser(headers);
        shopServiceService.delete(id, user);
    }

    @GetMapping
    public List<ShopService> getAllShopServicesOrByIdOrFilterByNameAndPrice(
            @RequestHeader HttpHeaders headers,
            @RequestParam(name = "id", required = false) Optional<Integer> id,
            @RequestParam(name = "name", required = false) Optional<String> name,
            @RequestParam(name = "price", required = false) Optional<Float> price) {
        User user = authenticationHelper.tryGetUser(headers);
        if (id.isPresent() && (name.isPresent() || price.isPresent())){
            throw new OrderOfOperationsException("Search by ID should not be combined with anything else. " +
                    "Filter by sort and price can be separated or combined.");
        } else if (id.isPresent()) {
            List<ShopService> list = new ArrayList<>();
            list.add(shopServiceService.getById(id.get(), user));
            return list;
        } else if (name.isPresent() || price.isPresent()) {
            ShopServiceFilterParameters ssfs = shopServiceMapper.fromParametersListForEmployee(name, price);
            return shopServiceService.getShopServicesByNameAndPrice(ssfs, user);
        } else {
            return shopServiceService.getAll(user);
        }
    }

    @GetMapping("/customerservices")
    public List<ShopService> getAllServicesLinkedToACustomer(
            @RequestHeader HttpHeaders headers,
            @RequestParam(name = "vehicleId", required = false) Optional<Integer> vehicleId,
            @RequestParam(name = "startDate", required = false) Optional<String> startDate,
            @RequestParam(name = "endDate", required = false) Optional<String> endDate){
        User user = authenticationHelper.tryGetUser(headers);
        ShopServiceFilterParameters ssfs = shopServiceMapper.fromParametersListForCustomer(vehicleId,startDate,endDate);
        return shopServiceService.getAllServicesLinkedToACustomer(user, ssfs);
    }
}
