package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.ServiceOrderDto;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ServiceOrderFilterParameters;
import com.example.smartgarage.models.mappers.ServiceOrderMapper;
import com.example.smartgarage.services.contracts.ServiceOrderService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/serviceorders")
public class ServiceOrderController {

    private final ServiceOrderService serviceOrderService;
    private final ServiceOrderMapper serviceOrderMapper;
    private final AuthenticationHelper authenticationHelper;

    public ServiceOrderController(ServiceOrderService serviceOrderService,
                                  ServiceOrderMapper serviceOrderMapper,
                                  AuthenticationHelper authenticationHelper) {
        this.serviceOrderService = serviceOrderService;
        this.serviceOrderMapper = serviceOrderMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public ServiceOrder createServiceOrder(@RequestHeader HttpHeaders headers,
                                           @Valid @RequestBody ServiceOrderDto serviceOrderDto) {

        User user = authenticationHelper.tryGetUser(headers);
        ServiceOrder serviceOrder = serviceOrderMapper.fromDtoToObjectWhenCreateOrUpdate(serviceOrderDto);
        serviceOrderService.create(serviceOrder, user);
        return serviceOrder;
    }

    @PostMapping(value = "/front-end", consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ServiceOrder createServiceOrderFrontEnd(@RequestHeader HttpHeaders headers,
                                            ServiceOrderDto serviceOrderDto) {

        User user = authenticationHelper.tryGetUser(headers);
        ServiceOrder serviceOrder = serviceOrderMapper.fromDtoToObjectWhenCreateOrUpdate(serviceOrderDto);
        serviceOrderService.createFromFrontEnd(serviceOrder, user);
        return serviceOrder;
    }

    @GetMapping
    public List<ServiceOrder> getAllServiceOrdersOrById(@RequestHeader HttpHeaders headers,
                                                        @RequestParam(name = "id", required = false)
                                                                Optional<Integer> id) {

        User user = authenticationHelper.tryGetUser(headers);
        if (id.isPresent()) {
            List<ServiceOrder> list = new ArrayList<>();
            list.add(serviceOrderService.getById(id.get(), user));
            return list;
        } else {
            return serviceOrderService.getAll(user);
        }
    }

    @PutMapping(value = "/front-end", consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateServiceOrderFrontEnd(@RequestHeader HttpHeaders headers,
                                    ServiceOrderDto serviceOrderDto) {
        User user = authenticationHelper.tryGetUser(headers);
        ServiceOrder serviceOrder = serviceOrderMapper.fromDtoToObjectWhenCreateOrUpdate(serviceOrderDto);
        serviceOrderService.update(serviceOrder, user);
    }
    @PutMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateServiceOrder(@RequestHeader HttpHeaders headers,
                                   @Valid @RequestBody ServiceOrderDto serviceOrderDto) {
        User user = authenticationHelper.tryGetUser(headers);
        ServiceOrder serviceOrder = serviceOrderMapper.fromDtoToObjectWhenCreateOrUpdate(serviceOrderDto);
        serviceOrderService.update(serviceOrder, user);
    }


    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteServiceOrder(@RequestHeader HttpHeaders headers,
                                   @RequestParam(name = "id") int id) {
        User user = authenticationHelper.tryGetUser(headers);
        serviceOrderService.delete(id, user);
    }

    @GetMapping("/getByMonth")
    public List<ServiceOrder> getAllServicesForMonth(@RequestHeader HttpHeaders headers,
                                                     @RequestParam(name = "year") String year,
                                                     @RequestParam(name = "month") String month){
        User user = authenticationHelper.tryGetUser(headers);
        return serviceOrderService.getAllServicesForMonth(year, month, user);
    }

    @GetMapping("/getbyvisitid")
    public List<ServiceOrder> getAllServicesByVisitId(@RequestHeader HttpHeaders headers,
                                                      @RequestParam(name = "visitId") Integer visitId){
        User user = authenticationHelper.tryGetUser(headers);
        return serviceOrderService.getAllServicesByVisitId(visitId, user);
    }

    @GetMapping("/userid")
    public List<ServiceOrder> getAllServicesByUserId(@RequestHeader HttpHeaders headers,
                                                     @RequestParam(name = "userId") Integer userId){
        User user = authenticationHelper.tryGetUser(headers);
        return serviceOrderService.getAllServicesByUserId(userId, user);
    }

    @GetMapping("/filter")
    public List<ServiceOrder> filter(@RequestHeader HttpHeaders headers,
                                     @RequestParam(name = "vehicleId", required = false) Optional<Integer> vehicleId,
                                     @RequestParam(name = "startDate", required = false) Optional<String> startDate){
        User user = authenticationHelper.tryGetUser(headers);
        ServiceOrderFilterParameters sofp = serviceOrderMapper.filterFromParametersList(vehicleId, startDate);
        return serviceOrderService.filterByVehicleIdAndOrStartDate(user, sofp);
    }
}
