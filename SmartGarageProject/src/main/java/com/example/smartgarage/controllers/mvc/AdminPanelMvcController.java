package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.exceptions.AuthenticationFailureException;
import com.example.smartgarage.exceptions.DuplicateEntityException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.exceptions.UnauthorizedOperationException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.mappers.*;
import com.example.smartgarage.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.exceptions.TemplateInputException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin-panel")
public class AdminPanelMvcController {

    private final UserMapper userMapper;
    private final UserService userService;
    private final VisitMapper visitMapper;
    private final VisitService visitService;
    private final BrandMapper brandMapper;
    private final BrandService brandService;
    private final ModelMapper modelMapper;
    private final ModelService modelService;
    private final VehicleMapper vehicleMapper;
    private final VehicleService vehicleService;
    private final ServiceOrderMapper serviceOrderMapper;
    private final ServiceOrderService serviceOrderService;
    private final AuthenticationHelper authenticationHelper;
    private final RestartPasswordService restartPasswordService;
    private final ShopServiceService shopServiceService;

    @Autowired
    public AdminPanelMvcController(UserMapper userMapper, UserService userService,
                                   VisitService visitService, VehicleMapper vehicleMapper,
                                   VehicleService vehicleService, VisitMapper visitMapper,
                                   AuthenticationHelper authenticationHelper, BrandMapper brandMapper,
                                   BrandService brandService, ModelMapper modelMapper,
                                   ModelService modelService, RestartPasswordService restartPasswordService,
                                   ServiceOrderMapper serviceOrderMapper, ServiceOrderService serviceOrderService,
                                   ShopServiceService shopServiceService) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.visitService = visitService;
        this.vehicleMapper = vehicleMapper;
        this.vehicleService = vehicleService;
        this.visitMapper = visitMapper;
        this.authenticationHelper = authenticationHelper;
        this.brandMapper = brandMapper;
        this.brandService = brandService;
        this.modelMapper = modelMapper;
        this.modelService = modelService;
        this.restartPasswordService = restartPasswordService;
        this.serviceOrderMapper = serviceOrderMapper;
        this.serviceOrderService = serviceOrderService;
        this.shopServiceService = shopServiceService;
    }

    @ModelAttribute("vehicleMake")
    public List<Brand> populateVehicleMakes(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        return brandService.getAll(currentUser);
    }

    @ModelAttribute("vehicleModel")
    public List<Model> populateVehicleModels(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        return modelService.getAll(currentUser);
    }

    @ModelAttribute("shopServices")
    public List<ShopService> populateServices(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        return shopServiceService.getAll(currentUser);
    }

    @ModelAttribute("activeVisits")
    public List<Visit> populateActiveVisits(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        return visitService.getAllActiveVisits(currentUser);
    }

    @GetMapping
    public String showRegisterPage(org.springframework.ui.Model model, HttpSession session) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            if (currentUser.isCustomer()) {
                return "redirect:/notFound";
            }
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }


        model.addAttribute("userRegisterDto", new UserDto());
        model.addAttribute("vehicleDto", new VehicleDto());
        model.addAttribute("visitDto", new VisitDto());
        model.addAttribute("brandDto", new BrandDto());
        model.addAttribute("modelDto", new ModelDto());
        model.addAttribute("passRestartDto", new PassRestartDto());
        model.addAttribute("serviceOrderDto", new ServiceOrderDto());
        model.addAttribute("visitIdDto", new VisitIdDto());

        return "admin-panel";
    }

    //Here - Admins create new Users
    @PostMapping("/register/user")
    public String handleRegister(@Valid @ModelAttribute("userRegisterDto")
                                         UserDto userRegDto,
                                 BindingResult bindingResult,
                                 HttpSession session) throws BindException{

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {

            User userToVerify = authenticationHelper.tryGetUser(session);
            User userToRegister = userMapper.fromRegistrationDto(userRegDto);
            userService.registerUser(userToRegister, userToVerify);

        }
//        catch (Exception e) {
//            bindingResult.rejectValue("email", "email_error", e.getMessage());
//            return "redirect:/admin-panel";
//        }
        catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        } catch (TemplateInputException e) {
            bindingResult.rejectValue("phone", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        }

        return "redirect:/admin-panel";
    }


    @PostMapping("/register/vehicle")
    public String handleRegisterVehicle(@Valid @ModelAttribute("vehicleDto")
                                                VehicleDto vehicleDto,
                                        BindingResult bindingResult,
                                        HttpSession session) throws BindException {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User userToVerify = authenticationHelper.tryGetUser(session);
            Vehicle vehicleToCreate = vehicleMapper.fromDtoToObjectWhenCreateOrUpdate(vehicleDto);
            vehicleService.create(vehicleToCreate, userToVerify);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("vin", "vehicleVin", e.getMessage());
            return "redirect:/admin-panel";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }

        return "redirect:/admin-panel";
    }

    @PostMapping("/register/visit")
    public String handleRegisterVisit(@Valid @ModelAttribute("visitDto")
                                              VisitDto visitDto,
                                      BindingResult bindingResult,
                                      HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User userToVerify = authenticationHelper.tryGetUser(session);
            Visit visitToCreate = visitMapper.fromHtmlDto(visitDto);
            visitService.create(visitToCreate, userToVerify);

        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }

        return "redirect:/admin-panel";
    }

    @PostMapping("/register/make")
    public String handleRegisterMake(@Valid @ModelAttribute("brandDto")
                                             BrandDto brandDto,
                                     BindingResult bindingResult,
                                     HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User userToVerify = authenticationHelper.tryGetUser(session);
            Brand brandToCreate = brandMapper.fromDtoToObjectWhenCreateOrUpdate(brandDto);
            brandService.create(brandToCreate, userToVerify);

        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("brandName", "brandName", e.getMessage());
            return "redirect:/admin-panel";
        }

        return "redirect:/admin-panel";
    }

    @PostMapping("/register/model")
    public String handleRegisterModel(@Valid @ModelAttribute("modelDto")
                                              ModelDto modelDto,
                                      BindingResult bindingResult,
                                      HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User userToVerify = authenticationHelper.tryGetUser(session);
            Model modelToCreate =
                    modelMapper.fromDtoToObjectWhenCreateOrUpdate(modelDto);
            modelService.create(modelToCreate, userToVerify);

        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("model_name", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("brand_id", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        }

        return "redirect:/admin-panel";
    }

    //here - Employees can send customers emails to restart passwords
    @PostMapping("/create-pass-restart")
    public String handlePassRestart(@Valid @ModelAttribute("passRestartDto")
                                            PassRestartDto passRestartDto,
                                    BindingResult bindingResult,
                                    HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            restartPasswordService.sendResetPasswordMail(currentUser, passRestartDto);
        } catch (AuthenticationFailureException | EntityNotFoundException e) {
            bindingResult.rejectValue("emailId", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        }

        return "redirect:/admin-panel";
    }

    //here - Employees can create service orders
    @PostMapping("/register/service-order")
    public String handleCreateServiceOrder(@Valid @ModelAttribute("serviceOrderDto")
                                                   ServiceOrderDto serviceOrderDto,
                                           BindingResult bindingResult,
                                           HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {

            User userToVerify = authenticationHelper.tryGetUser(session);
            ServiceOrder orderToCreate =
                    serviceOrderMapper.fromDtoToObjectWhenCreateOrUpdate(serviceOrderDto);

            serviceOrderService.createAndSendMail(orderToCreate, userToVerify);

        } catch (AuthenticationFailureException | EntityNotFoundException e) {
            bindingResult.rejectValue("serviceId", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        }

        return "redirect:/admin-panel";
    }

    //here - Employees send mail with all service orders for a visit to its customer
    @PostMapping("/send/all-orders-for-visit")
    public String handleAllOrdersOfAVisit(@Valid @ModelAttribute("visitIdDto")
                                                  VisitIdDto visitIdDto,
                                          BindingResult bindingResult,
                                          HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "redirect:/admin-panel";
        }

        try {

            User userToVerify = authenticationHelper.tryGetUser(session);

            visitService.sendMailOfAllOrdersForAVisit(visitIdDto.getVisitId(), userToVerify);

        } catch (AuthenticationFailureException | EntityNotFoundException e) {
            bindingResult.rejectValue("visitId", "auth_error", e.getMessage());
            return "redirect:/admin-panel";
        }

        return "redirect:/admin-panel";
    }

}
