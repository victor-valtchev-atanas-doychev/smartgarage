package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.models.Model;
import com.example.smartgarage.models.ModelDto;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.mappers.ModelMapper;
import com.example.smartgarage.services.contracts.ModelService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/models")
public class ModelController {

    private final ModelService modelService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    public ModelController(ModelService modelService,
                           ModelMapper modelMapper,
                           AuthenticationHelper authenticationHelper) {

        this.modelService = modelService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Model createModel(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody ModelDto modelDto) {

        User user = authenticationHelper.tryGetUser(headers);
        Model model = modelMapper.fromDtoToObjectWhenCreateOrUpdate(modelDto);
        modelService.create(model, user);
        return model;
    }


    // To be merged with getById like in VehicleController.
    @GetMapping
    public List<Model> getAllModels(@RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        return modelService.getAll(user);
    }


    @PutMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public Model updateModel(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody ModelDto modelDto) {

        User user = authenticationHelper.tryGetUser(headers);
        Model model = modelMapper.fromDtoToObjectWhenCreateOrUpdate(modelDto);
        modelService.update(model, user);
        return model;
    }

    // To be requested HttpHeaders, to be added UserAuthentication.
    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteModel(@RequestHeader HttpHeaders headers,
                            @RequestParam(name = "id") int id) {

        User user = authenticationHelper.tryGetUser(headers);
        modelService.delete(id, user);
    }
}
