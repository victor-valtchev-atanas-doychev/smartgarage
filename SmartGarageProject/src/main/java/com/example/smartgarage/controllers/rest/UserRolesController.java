package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserRoles;
import com.example.smartgarage.models.UserRolesDto;
import com.example.smartgarage.models.mappers.UserRoleMapper;
import com.example.smartgarage.services.contracts.UserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/roles")
public class UserRolesController {

    private final UserRolesService userRolesService;
    private final UserRoleMapper userRoleMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserRolesController(UserRolesService userRolesService,
                               UserRoleMapper userRoleMapper,
                               AuthenticationHelper authenticationHelper) {

        this.userRolesService = userRolesService;
        this.userRoleMapper = userRoleMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/all")
    public List<UserRoles> getAll(@RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        return userRolesService.getAll(user);
    }

    @PostMapping("/add")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void addUserRoles(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody UserRolesDto userRoleDto) {

        User userToVerify = authenticationHelper.tryGetUser(headers);
        UserRoles userRolesToAdd = userRoleMapper.fromDto(userRoleDto);
        userRolesService.addRole(userToVerify, userRolesToAdd);

    }

    @DeleteMapping("/delete")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteUserRoles(@RequestHeader HttpHeaders headers,
                                @Valid @RequestBody UserRolesDto userRoleDto) {

        User userToVerify = authenticationHelper.tryGetUser(headers);
        UserRoles userRolesToDelete = userRoleMapper.fromDto(userRoleDto);
        userRolesService.deleteRole(userToVerify, userRolesToDelete);
    }
}
