package com.example.smartgarage.controllers;

import com.example.smartgarage.exceptions.AuthenticationFailureException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.exceptions.UnauthorizedOperationException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "The requested resource requires authorization. Please enter email.");
        }
        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getUserByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username.");
        }
    }

    public User verifyAuthorization(HttpSession session, String role) {
        User user = tryGetUser(session);

        Set<String> userRoles = user
                .getRoles()
                .stream()
                .map(r -> r.getRole().toLowerCase())
                .collect(Collectors.toSet());

        if (!userRoles.contains(role.toLowerCase())) {
            throw new UnauthorizedOperationException("Users does not have the required authorization.");
        }

        return user;
    }

    public void verifyAuthentication(String email, String password) {
        try {
            User user = userService.getUserByEmail(email);

            if (!user.isEnabled()){
                throw new AuthenticationFailureException("This account is inactive.");
            }

            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException("Wrong password.");
            }
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException("Wrong email.");
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUserEmail = (String) session.getAttribute("currentUserEmail");

        if (currentUserEmail == null) {
            throw new UnauthorizedOperationException("No logged in user.");
        }

        try {
            return userService.getUserByEmail(currentUserEmail);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("No logged in user.");
        }
    }
}
