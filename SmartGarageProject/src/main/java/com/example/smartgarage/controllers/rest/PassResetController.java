package com.example.smartgarage.controllers.rest;


import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.models.PassRestartRequest;
import com.example.smartgarage.models.User;
import com.example.smartgarage.services.contracts.RestartPasswordService;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/passreset")
public class PassResetController {

    private final RestartPasswordService restartPasswordService;
    private final AuthenticationHelper authenticationHelper;

    public PassResetController(RestartPasswordService restartPasswordService, AuthenticationHelper authenticationHelper) {
        this.restartPasswordService = restartPasswordService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/all")
    public List<PassRestartRequest> getAllPassRestartRequest(@RequestHeader HttpHeaders headers){
        User user = authenticationHelper.tryGetUser(headers);
        return restartPasswordService.getAllPassRestartRequest(user);
    }

    @GetMapping("/uncompleted-requests")
    public List<PassRestartRequest> getAllUncompletedRestartRequests(@RequestHeader HttpHeaders headers){
        User user = authenticationHelper.tryGetUser(headers);
        return restartPasswordService.getUncompletedRequests(user);
    }
}
