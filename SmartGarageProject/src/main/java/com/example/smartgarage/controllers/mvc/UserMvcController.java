package com.example.smartgarage.controllers.mvc;


import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.exceptions.UnauthorizedOperationException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    public UserMvcController(UserService userService,
                             AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        User employee = userService.getUserByEmail("a.d@gmail.com");
        return userService.getAll(employee);
    }

    @GetMapping
    public String getAllUsers(Model model, HttpSession session){
        User currentUser;

        try{
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            if (!currentUser.isEmployee()){
                return "notFound";
            }
        } catch (UnauthorizedOperationException e){
            return "notFound";
        }

        model.addAttribute("users", userService.getAll(currentUser));
        return "users";
    }
}