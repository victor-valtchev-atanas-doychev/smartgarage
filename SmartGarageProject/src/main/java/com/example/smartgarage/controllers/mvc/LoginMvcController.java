package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.exceptions.AuthenticationFailureException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserLoginDto;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping
public class LoginMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public LoginMvcController(UserService userService,
                              AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDto", new UserLoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto")
                              UserLoginDto userLoginDto,
                              BindingResult bindingResult,
                              HttpSession session,
                              Model model) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(
                    userLoginDto.getEmail(), userLoginDto.getPassword());

            User user = userService.getUserByEmail(userLoginDto.getEmail());
            model.addAttribute("currentUser", user);

            session.setAttribute("currentUserEmail", user.getEmail());
            session.setAttribute("currentUser", user);
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("password", "auth_error", e.getMessage());
            return "login";
        }

        return "redirect:/";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUserEmail");
        return "redirect:/";
    }
}
