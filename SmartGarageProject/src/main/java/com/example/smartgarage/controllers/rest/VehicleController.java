package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.VehicleDto;
import com.example.smartgarage.models.mappers.VehicleMapper;
import com.example.smartgarage.services.contracts.UserService;
import com.example.smartgarage.services.contracts.VehicleService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;
    private final VehicleMapper vehicleMapper;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;

    public VehicleController(VehicleService vehicleService,
                             VehicleMapper vehicleMapper,
                             AuthenticationHelper authenticationHelper, UserService userService) {

        this.vehicleService = vehicleService;
        this.vehicleMapper = vehicleMapper;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Vehicle createVehicle(@RequestHeader HttpHeaders headers,
                                 @Valid @RequestBody VehicleDto vehicleDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Vehicle vehicle = vehicleMapper.fromDtoToObjectWhenCreateOrUpdate(vehicleDto);
        vehicleService.create(vehicle, user);
        return vehicle;
    }

    @GetMapping
    public List<Vehicle> getAllVehiclesOrByIdOrByOwner (@RequestHeader HttpHeaders headers,
                                                       @RequestParam(name = "id",required = false) Optional<Integer> id,
                                                       @RequestParam(name = "ownerId",required = false) Optional<Integer> ownerId){
        User user = authenticationHelper.tryGetUser(headers);
        if (id.isPresent()){
            List<Vehicle> list = new ArrayList<>();
            list.add(vehicleService.getById(id.get(), user));
            return list;
        } else if (ownerId.isPresent()){
            userService.getById(ownerId.get(), user);
            return vehicleService.getByOwner(ownerId.get(), user);
        } else {
            return vehicleService.getAll(user);
        }
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateVehicle(@RequestHeader HttpHeaders headers,
                              @Valid @RequestBody VehicleDto vehicleDto){
        User user = authenticationHelper.tryGetUser(headers);
        Vehicle vehicle = vehicleMapper.fromDtoToObjectWhenCreateOrUpdate(vehicleDto);
        vehicleService.update(vehicle, user);
    }

    @PutMapping(value = "/front-end", consumes = "application/x-www-form-urlencoded;charset=UTF-8")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateVehicleFrontEnd(@RequestHeader HttpHeaders headers,
                               VehicleDto vehicleDto){
        User user = authenticationHelper.tryGetUser(headers);
        Vehicle vehicle = vehicleMapper.fromDtoToObjectWhenCreateOrUpdate(vehicleDto);
        vehicleService.update(vehicle, user);
    }


    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteVehicle(@RequestHeader HttpHeaders headers,
                              @RequestParam(name = "id") int id){
        User user = authenticationHelper.tryGetUser(headers);
        vehicleService.delete(id, user);
    }

    @GetMapping("/getbymonth")
    public List<Vehicle> getAllVehiclesByMonth(@RequestHeader HttpHeaders headers,
                                               @RequestParam(name = "year") String year,
                                               @RequestParam(name = "month") String month){
        User user = authenticationHelper.tryGetUser(headers);
        return vehicleService.getAllVehiclesByMonth(year, month, user);
    }

}
