package com.example.smartgarage.controllers.mvc;


import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.exceptions.UnauthorizedOperationException;
import com.example.smartgarage.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/garagepanel")
public class GaragePanelMvcController {

    private final AuthenticationHelper authenticationHelper;

    public GaragePanelMvcController(AuthenticationHelper authenticationHelper) {
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showEmployeePanel(Model model, HttpSession session){

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            authenticationHelper.verifyAuthorization(session, "Employee");
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }

        return "garagepanelV2";
    }

    @GetMapping("/v1")
    public String showEmployeePanelV2(Model model, HttpSession session){

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            authenticationHelper.verifyAuthorization(session, "Employee");
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/notFound";
        }

        return "garagepanel";
    }
}
