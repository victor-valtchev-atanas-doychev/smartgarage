package com.example.smartgarage.controllers.rest;


import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.models.Brand;
import com.example.smartgarage.models.BrandDto;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.mappers.BrandMapper;
import com.example.smartgarage.services.contracts.BrandService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/brands")
public class BrandController {

    private final BrandService brandService;
    private final BrandMapper brandMapper;
    private final AuthenticationHelper authenticationHelper;

    public BrandController(BrandService brandService,
                           BrandMapper brandMapper,
                           AuthenticationHelper authenticationHelper) {

        this.brandService = brandService;
        this.brandMapper = brandMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Brand createBrand(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody BrandDto brandDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Brand brand = brandMapper.fromDtoToObjectWhenCreateOrUpdate(brandDto);
        brandService.create(brand, user);
        return brand;
    }

    @GetMapping
    public List<Brand> getAllBrandsOrById(@RequestHeader HttpHeaders headers,
                                          @RequestParam(name = "id", required = false)
                                                  Optional<Integer> id) {
        User user = authenticationHelper.tryGetUser(headers);
        if (id.isPresent()) {
            List<Brand> list = new ArrayList<>();
            list.add(brandService.getById(id.get(), user));
            return list;
        }
        return brandService.getAll(user);
    }

    @PutMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void updateBrand(@RequestHeader HttpHeaders headers,
                            @Valid @RequestBody BrandDto brandDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Brand brand = brandMapper.fromDtoToObjectWhenCreateOrUpdate(brandDto);
        brandService.update(brand, user);
    }

    @DeleteMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteBrand(@RequestHeader HttpHeaders headers,
                            @RequestParam int id) {
        User user = authenticationHelper.tryGetUser(headers);
        brandService.delete(id, user);
    }
}
