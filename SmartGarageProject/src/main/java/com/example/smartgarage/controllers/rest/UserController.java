package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.controllers.AuthenticationHelper;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserDto;
import com.example.smartgarage.models.filterParameters.CustomersSortParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;
import com.example.smartgarage.models.mappers.UserMapper;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService,
                          UserMapper userMapper,
                          AuthenticationHelper authenticationHelper) {

        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/all")
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getAll(user);
    }

    @GetMapping("/customers")
    public List<User> getAllCustomersWithSort(
            @RequestHeader HttpHeaders headers,
            @RequestParam(name = "nameOrder", required = false) Optional<String> nameOrder,
            @RequestParam(name = "dateOrder", required = false) Optional<String> dateOrder) {

        User user = authenticationHelper.tryGetUser(headers);
        CustomersSortParameters csp = userMapper.sortFromParametersList(nameOrder, dateOrder);
        return userService.getAllCustomersWithSort(user, csp);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public UserDto create(@RequestHeader HttpHeaders headers,
                          @Valid @RequestBody UserDto userDto) {

        User userToVerify = authenticationHelper.tryGetUser(headers);
        User userToCreate = userMapper.fromDto(userDto);
        userService.create(userToCreate, userToVerify);
        return userDto;
    }


    @PutMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void update(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody UserDto userDto) {

        User userToVerify = authenticationHelper.tryGetUser(headers);
        User userToUpdate = userMapper.fromDto(userDto);
        userService.update(userToUpdate, userToVerify);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {

        User userToVerify = authenticationHelper.tryGetUser(headers);
        userService.delete(id, userToVerify);
    }

    @GetMapping("/customers/filter")
    public List<User> filter(@RequestHeader HttpHeaders headers,
                            @RequestParam(name = "firstName", required = false) Optional<String> firstName,
                            @RequestParam(name = "lastName", required = false) Optional<String> lastName,
                            @RequestParam(name = "email", required = false) Optional<String> email,
                            @RequestParam(name = "phone", required = false) Optional<String> phone,
                            @RequestParam(name = "model", required = false) Optional<String> model,
                            @RequestParam(name = "make", required = false) Optional<String> make,
                            @RequestParam(name = "visitStart", required = false) Optional<String> visitStart,
                            @RequestParam(name = "visitEnd", required = false) Optional<String> visitEnd) {

        if (firstName.isEmpty() && lastName.isEmpty() && email.isEmpty() && phone.isEmpty()
                && model.isEmpty() && make.isEmpty() && visitStart.isEmpty() && visitEnd.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Parameters required.");
        }

        User userToVerify = authenticationHelper.tryGetUser(headers);
        UserFilterParameters ufs = userMapper.filterFromParametersList(
                firstName, lastName, email, phone, model, make, visitStart, visitEnd);

        return userService.filter(userToVerify, ufs);
    }

    @PostMapping("/customers/register")
    @ResponseStatus(value = HttpStatus.CREATED)
    public String registerUser(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody UserDto userDto) {

        User userToVerify = authenticationHelper.tryGetUser(headers);
        User userToCreate = userMapper.fromDto(userDto);
        return userService.registerUser(userToCreate, userToVerify);
    }

    @GetMapping("/customers/confirm")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public String confirmUser(@RequestParam(name = "token") String token){
        userService.confirmUser(token);
        return "Profile confirmed";
    }
}
