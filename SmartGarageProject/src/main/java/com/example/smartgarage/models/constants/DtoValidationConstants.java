package com.example.smartgarage.models.constants;

public class DtoValidationConstants {

    public static final String USER_NAME_LENGTH_MESSAGE =
            "Name should be between 2 and 20 symbols.";

    public static final String USER_PHONE_LENGTH_MESSAGE =
            "Phone should be 10 digits long.";

    public static final String USER_EMAIL_LENGTH_MESSAGE =
            "Email should be between 5 and 40 symbols.";

    public static final String USER_PASSWORD_LENGTH_MESSAGE =
            "Password should be at least 8 signs.";

    public static final String BRAND_NAME_LENGTH_MESSAGE =
            "Brand should be no longer than 30 signs.";

    public static final String MODEL_NAME_LENGTH_MESSAGE =
            "Model should be no longer than 30 signs.";

    public static final String SERVICE_NAME_LENGTH_MESSAGE =
            "Service should be no longer than 50 signs.";

    public static final String VEHICLE_PLATE_LENGTH_MESSAGE =
            "Vehicle license plate should be 7 or 8 signs.";

    public static final String VEHICLE_VIN_LENGTH_MESSAGE =
            "Vehicle VIN should be 17 signs.";
}
