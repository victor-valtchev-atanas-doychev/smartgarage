package com.example.smartgarage.models;

import javax.validation.constraints.*;

public class UserRolesDto {

    @Positive
    @NotNull
    private int user_id;

    @Positive
    @NotNull
    private int role_id;

    public UserRolesDto() {
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }
}