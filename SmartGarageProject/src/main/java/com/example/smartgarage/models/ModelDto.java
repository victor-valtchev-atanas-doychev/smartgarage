package com.example.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.example.smartgarage.models.constants.DtoValidationConstants.*;

public class ModelDto {

    public ModelDto() {
    }

    private int id;

    @NotNull
    @Positive
    private int brand_id;

    @NotNull
    @Size(max = 30, message = MODEL_NAME_LENGTH_MESSAGE)
    private String model_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }
}
