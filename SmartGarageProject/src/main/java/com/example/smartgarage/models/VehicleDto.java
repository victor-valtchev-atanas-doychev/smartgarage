package com.example.smartgarage.models;

import com.example.smartgarage.models.util.CorrectLicensePlate;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.example.smartgarage.models.constants.DtoValidationConstants.*;

public class VehicleDto {

    public VehicleDto() {
    }

    private int id;

    @NotNull
    @Size(min = 7, max = 8, message = VEHICLE_PLATE_LENGTH_MESSAGE)
    @CorrectLicensePlate
    private String license_plate;

    @NotNull
    @Size(min = 17,max = 17, message = VEHICLE_VIN_LENGTH_MESSAGE)
    private String vin;

    @NotNull
    @Positive
    private int year;

    @NotNull
    @Positive
    private int owner_id;

    @NotNull
    @Positive
    private int model_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicense_plate() {
        return license_plate;
    }

    public void setLicense_plate(String license_plate) {
        this.license_plate = license_plate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public int getModel_id() {
        return model_id;
    }

    public void setModel_id(int model_id) {
        this.model_id = model_id;
    }
}
