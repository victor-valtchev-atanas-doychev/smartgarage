package com.example.smartgarage.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "restart_password_requests")
public class PassRestartRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "date_requested")
    private LocalDateTime requestDate = LocalDateTime.now();

    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "date_restarted")
    private LocalDateTime restartDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setRequestDate(LocalDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    public LocalDateTime getRestartDate() {
        return restartDate;
    }

    public void setRestartDate(LocalDateTime restartDate) {
        this.restartDate = restartDate;
    }
}
