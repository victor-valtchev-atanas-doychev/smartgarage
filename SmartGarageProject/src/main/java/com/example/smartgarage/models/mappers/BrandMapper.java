package com.example.smartgarage.models.mappers;

import com.example.smartgarage.models.Brand;
import com.example.smartgarage.models.BrandDto;
import org.springframework.stereotype.Component;

@Component
public class BrandMapper {

    public Brand fromDtoToObjectWhenCreateOrUpdate(BrandDto brandDto){
        Brand brand = new Brand();
        brand.setId(brandDto.getId());
        brand.setBrandName(brandDto.getBrandName());
        return brand;
    }

}
