package com.example.smartgarage.models.mappers;

import com.example.smartgarage.models.Model;
import com.example.smartgarage.models.ModelDto;
import com.example.smartgarage.repositories.contracts.BrandRepository;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {

    private final BrandRepository brandRepository;

    public ModelMapper(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    public Model fromDtoToObjectWhenCreateOrUpdate(ModelDto modelDto){
        Model model = new Model();
        model.setId(modelDto.getId());
        model.setModelName(modelDto.getModel_name());
        model.setBrand(brandRepository.getById(modelDto.getBrand_id()));
        return model;
    }
}
