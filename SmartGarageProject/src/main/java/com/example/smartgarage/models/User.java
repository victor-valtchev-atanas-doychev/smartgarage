package com.example.smartgarage.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    private static final String ADMIN_ROLE_KEY = "Admin";
    private static final String CUSTOMER_ROLE_KEY = "Customer";
    private static final String EMPLOYEE_ROLE_KEY = "Employee";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;


    @Column(name = "enabled")
    private boolean enabled;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enables) {
        this.enabled = enables;
    }

    @JsonIgnore
    public boolean isCustomer() {
        return getRoles()
                .stream()
                .anyMatch(r -> r.getRole().equals(CUSTOMER_ROLE_KEY)); }

    @JsonIgnore
    public boolean isEmployee() {
        return getRoles()
                .stream()
                .anyMatch(r -> r.getRole().equals(EMPLOYEE_ROLE_KEY));
    }

    @JsonIgnore
    public boolean isAdmin() {
        return getRoles()
                .stream()
                .anyMatch(r -> r.getRole().equals(ADMIN_ROLE_KEY)); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return getId() == user.getId() &&
                getEmail().equals(user.getEmail())  &&
                getFirstName().equals(user.getFirstName()) &&
                getLastName().equals(user.getLastName()) &&
                getPhone().equals(user.getPhone());
    }
}
