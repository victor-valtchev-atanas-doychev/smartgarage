package com.example.smartgarage.models.filterParameters;

import java.util.Optional;

public class ServiceOrderFilterParameters {
    private Optional<Integer> vehicleId;
    private Optional<String> startDate;

    public ServiceOrderFilterParameters() {
    }

    public Optional<Integer> getVehicleId() {
        return vehicleId;
    }

    public ServiceOrderFilterParameters setVehicleId(Optional<Integer> vehicleId) {
        this.vehicleId = vehicleId;
        return this;
    }

    public Optional<String> getStartDate() {
        return startDate;
    }

    public ServiceOrderFilterParameters setStartDate(Optional<String> startDate) {
        this.startDate = startDate;
        return this;
    }
}
