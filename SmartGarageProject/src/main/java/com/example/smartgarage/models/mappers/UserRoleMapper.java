package com.example.smartgarage.models.mappers;


import com.example.smartgarage.models.UserRoles;
import com.example.smartgarage.models.UserRolesDto;
import com.example.smartgarage.repositories.contracts.RolesRepository;
import com.example.smartgarage.repositories.contracts.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserRoleMapper {

    private final UserRepository userRepository;
    private final RolesRepository rolesRepository;

    public UserRoleMapper(UserRepository userRepository, RolesRepository rolesRepository) {
        this.userRepository = userRepository;
        this.rolesRepository = rolesRepository;
    }

    public UserRoles fromDto(UserRolesDto userRoleDto){
        UserRoles userRoles = new UserRoles();
        userRoles.setUser(userRepository.getById(userRoleDto.getUser_id()));
        userRoles.setRole(rolesRepository.getById(userRoleDto.getRole_id()));
        return userRoles;
    }
}
