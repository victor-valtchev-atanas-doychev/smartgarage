package com.example.smartgarage.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.example.smartgarage.models.constants.DtoValidationConstants.*;

public class PassRestartRequestDto {

    @NotNull
    @Email
    @Size(min = 5, max = 40, message = USER_EMAIL_LENGTH_MESSAGE)
    private String email;

    public PassRestartRequestDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
