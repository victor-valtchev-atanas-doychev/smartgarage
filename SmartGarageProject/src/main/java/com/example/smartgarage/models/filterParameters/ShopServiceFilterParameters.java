package com.example.smartgarage.models.filterParameters;
import java.util.Optional;

public class ShopServiceFilterParameters {

    private Optional<String> name;
    private Optional<Float> price;
    private Optional<Integer> vehicleId;
    private Optional<String> startDate;
    private Optional<String> endDate;

    public Optional<Integer> getVehicleId() {
        return vehicleId;
    }

    public ShopServiceFilterParameters setVehicleId(Optional<Integer> vehicleId) {
        this.vehicleId = vehicleId;
        return this;
    }

    public Optional<String> getStartDate() {
        return startDate;
    }

    public Optional<String> getEndDate() {
        return endDate;
    }

    public ShopServiceFilterParameters setEndDate(Optional<String> endDate) {
        this.endDate = endDate;
        return this;
    }

    public ShopServiceFilterParameters setStartDate(Optional<String> startDate) {
        this.startDate = startDate;
        return this;
    }

    public Optional<String> getName() {
        return name;
    }

    public ShopServiceFilterParameters setName(Optional<String> name) {
        this.name = name;
        return this;
    }

    public Optional<Float> getPrice() {
        return price;
    }

    public ShopServiceFilterParameters setPrice(Optional<Float> price) {
        this.price = price;
        return this;
    }


}

