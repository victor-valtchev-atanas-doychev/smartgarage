package com.example.smartgarage.models.filterParameters;

import java.util.Optional;

public class CustomersSortParameters {

    private Optional<String> nameOrder;
    private Optional<String> dateOrder;

    public CustomersSortParameters() {
    }

    public Optional<String> getNameOrder() {
        return nameOrder;
    }

    public CustomersSortParameters setNameOrder(Optional<String> nameOrder) {
        this.nameOrder = nameOrder;
        return this;
    }

    public Optional<String> getDateOrder() {
        return dateOrder;
    }

    public CustomersSortParameters setDateOrder(Optional<String> dateOrder) {
        this.dateOrder = dateOrder;
        return this;
    }
}
