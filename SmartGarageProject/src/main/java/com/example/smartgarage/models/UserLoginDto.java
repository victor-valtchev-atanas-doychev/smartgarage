package com.example.smartgarage.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.example.smartgarage.models.constants.DtoValidationConstants.*;

public class UserLoginDto {

    @NotNull
    @Email
    @Size(min = 5, max = 40, message = USER_EMAIL_LENGTH_MESSAGE)
    private String email;

    @NotNull
    @Size(min = 8, message = USER_PASSWORD_LENGTH_MESSAGE)
    private String password;

    public UserLoginDto() {
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
