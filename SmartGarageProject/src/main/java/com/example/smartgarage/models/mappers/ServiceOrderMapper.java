package com.example.smartgarage.models.mappers;

import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.ServiceOrderDto;
import com.example.smartgarage.models.filterParameters.ServiceOrderFilterParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;
import com.example.smartgarage.repositories.contracts.ShopServiceRepository;
import com.example.smartgarage.repositories.contracts.VisitRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Component
public class ServiceOrderMapper {

    private final ShopServiceRepository shopServiceRepository;
    private final VisitRepository visitRepository;

    public ServiceOrderMapper(ShopServiceRepository shopServiceRepository, VisitRepository visitRepository) {
        this.shopServiceRepository = shopServiceRepository;
        this.visitRepository = visitRepository;
    }

    public ServiceOrder fromDtoToObjectWhenCreateOrUpdate(ServiceOrderDto serviceOrderDto){
        ServiceOrder serviceOrder = new ServiceOrder();
        serviceOrder.setId(serviceOrderDto.getId());
        serviceOrder.setService(shopServiceRepository.getById(serviceOrderDto.getServiceId()));
        serviceOrder.setVisit(visitRepository.getById(serviceOrderDto.getVisitId()));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if (serviceOrderDto.getEndDate() != null){
            String fullDate = serviceOrderDto.getEndDate();
            LocalDateTime localDateTime = LocalDateTime.parse(fullDate, formatter);
            serviceOrder.setEndDate(localDateTime);
        }
        return serviceOrder;
    }

    public ServiceOrderFilterParameters filterFromParametersList(Optional<Integer> vehicleId,
                                                                 Optional<String> startDate) {
        return new ServiceOrderFilterParameters().setVehicleId(vehicleId).setStartDate(startDate);
    }
}
