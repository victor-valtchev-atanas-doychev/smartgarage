package com.example.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.example.smartgarage.models.constants.DtoValidationConstants.*;

public class ShopServiceDto {

    public ShopServiceDto() {
    }

    private int id;

    @NotNull
    @Size(max = 50, message = SERVICE_NAME_LENGTH_MESSAGE)
    private String serviceName;

    @NotNull
    @Positive
    private float price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
