package com.example.smartgarage.models;

import javax.validation.constraints.NotNull;

public class PassRestartFromUserDto {

    @NotNull
    private String password;

    @NotNull
    private String repeatPassword;

    public PassRestartFromUserDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }
}