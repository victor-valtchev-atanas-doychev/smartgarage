package com.example.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class VisitIdDto {

    @NotNull
    @Positive
    private int visitId;

    public VisitIdDto() {
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }
}
