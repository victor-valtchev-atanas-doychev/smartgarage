package com.example.smartgarage.models.mappers;

import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitDto;
import com.example.smartgarage.repositories.contracts.VehicleRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class VisitMapper {

    private final VehicleRepository vehicleRepository;

    public VisitMapper(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    public Visit fromDtoToObjectWhenCreateOrUpdate(VisitDto visitDto){
        Visit visit = new Visit();
        visit.setId(visitDto.getId());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if (visitDto.getEndDate() != null){
            String fullDate = visitDto.getEndDate();
            LocalDateTime localDateTime = LocalDateTime.parse(fullDate, formatter);
            visit.setEndDate(localDateTime);
        }
        if (visitDto.getStartDate() != null){
            String fullDate = visitDto.getStartDate();
            LocalDateTime localDateTime = LocalDateTime.parse(fullDate, formatter);
            visit.setStartDate(localDateTime);
        }
        visit.setVehicle(vehicleRepository.getById(visitDto.getVehicleId()));
        return visit;
    }

    public Visit fromHtmlDto(VisitDto visitDto) {

        Visit newVisit = new Visit();
        Vehicle vehicle = vehicleRepository.getById(visitDto.getVehicleId());

        newVisit.setVehicle(vehicle);
        newVisit.setStartDate(LocalDateTime.now());

        return newVisit;
    }
}
