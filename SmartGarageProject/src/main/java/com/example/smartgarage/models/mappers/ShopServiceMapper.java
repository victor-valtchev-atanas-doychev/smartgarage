package com.example.smartgarage.models.mappers;

import com.example.smartgarage.models.ShopService;
import com.example.smartgarage.models.ShopServiceDto;
import com.example.smartgarage.models.filterParameters.ShopServiceFilterParameters;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ShopServiceMapper {

    public ShopServiceMapper() {
    }

    public ShopService fromDtoToObjectWhenCreateOrUpdate(ShopServiceDto shopServiceDto){
        ShopService shopService = new ShopService();
        shopService.setId(shopServiceDto.getId());
        shopService.setName(shopServiceDto.getServiceName());
        shopService.setPrice(shopServiceDto.getPrice());
        return shopService;
    }

    public ShopServiceFilterParameters fromParametersListForEmployee(Optional<String> name,
                                                                     Optional<Float> price) {
        return new ShopServiceFilterParameters()
                .setName(name)
                .setPrice(price);

    }

    public ShopServiceFilterParameters fromParametersListForCustomer(Optional<Integer> vehicleId,
                                                                     Optional<String> startDate,
                                                                     Optional<String> endDate){
        return new ShopServiceFilterParameters()
                .setVehicleId(vehicleId)
                .setStartDate(startDate)
                .setEndDate(endDate);
    }
}
