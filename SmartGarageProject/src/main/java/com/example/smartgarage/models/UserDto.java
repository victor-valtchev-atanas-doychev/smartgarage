package com.example.smartgarage.models;

import javax.validation.constraints.*;

import static com.example.smartgarage.models.constants.DtoValidationConstants.*;

public class UserDto {

    private int id;

    @NotNull
    @Size(min = 2, max = 20, message = USER_NAME_LENGTH_MESSAGE)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 20, message = USER_NAME_LENGTH_MESSAGE)
    private String lastName;

    @NotNull
    @Positive
    @Size(min = 10, max = 10, message = USER_PHONE_LENGTH_MESSAGE)
    private String phone;

    @NotNull
    @Email
    @Size(min = 5, max = 40, message = USER_EMAIL_LENGTH_MESSAGE)
    private String email;

    @Size(min = 8, message = USER_PASSWORD_LENGTH_MESSAGE)
    private String password;

    public UserDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
