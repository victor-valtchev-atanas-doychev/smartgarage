package com.example.smartgarage.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "vehicles")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "license_plate")
    @Size(min = 7, max = 8)
    private String licensePlate;

    @NotNull
    @Column(name = "vin")
    @Size(min = 17,max = 17)
    private String vin;

    @NotNull
    @Column(name = "year")
    private int year;

    @OneToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    @OneToOne
    @JoinColumn(name = "model_id")
    private Model model;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User ownerID) {
        this.owner = ownerID;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model modelID) {
        this.model = modelID;
    }

}
