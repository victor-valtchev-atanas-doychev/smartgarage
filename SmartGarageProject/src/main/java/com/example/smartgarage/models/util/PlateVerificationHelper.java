package com.example.smartgarage.models.util;

import com.example.smartgarage.exceptions.WrongCarRegistrationPlateException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

public class PlateVerificationHelper implements ConstraintValidator<CorrectLicensePlate, String> {

    private final Set<String> bulgarianAllLetters = setBulgarianAllLetters();
    private final Set<String> bulgarianTownLetters = setBulgarianTownLetters();

    @Override
    public void initialize(CorrectLicensePlate constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return verifyBulgarianCivilCarPlate(value);
    }

    private boolean verifyBulgarianCivilCarPlate(String string) {

        String plate = string.replaceAll(" ", "").toUpperCase();
        int plateLength = 0;

        if (plate.length() == 7 || plate.length() == 8) {
            plateLength = plate.length();
        } else {
            throw new WrongCarRegistrationPlateException(
                    "Plate reg.No must be 7 or 8 symbols.");
        }

        int charRemoved = 0;

        switch (plateLength) {
            case 7:
                ifSevenDigits(plate, charRemoved);
                break;
            case 8:
                ifEightDigits(plate, charRemoved);
                break;
            //here we can add more cases for other types of plates such as: paid, diplomatic, military etc.
        }
        return true;
    }

    private void ifSevenDigits(String plate, int charRemoved) {

        //bottom
        if (plate.length() == 0) {
            return;
        }

        String currentLetter = String.valueOf(plate.charAt(0));

        //check single town letter
        if (charRemoved == 0) {
            if (bulgarianTownLetters.contains(currentLetter)) {
                charRemoved++;
                ifSevenDigits(plate.substring(1), charRemoved);
                return;
            } else {
                throw new WrongCarRegistrationPlateException(
                        "Please enter valid town letter.");
            }
        }

        //check the 4 digits in the middle
        if (charRemoved > 0 && charRemoved < 5) {
            if (isDigit(currentLetter)) {
                charRemoved++;
                ifSevenDigits(plate.substring(1), charRemoved);
                return;
            } else {
                throw new WrongCarRegistrationPlateException(
                        "Characters at position 2,3,4,5 must be digits.");
            }
        }

        //check last letters
        if (charRemoved > 4) {
            if (bulgarianAllLetters.contains(currentLetter)) {
                charRemoved++;
                ifSevenDigits(plate.substring(1), charRemoved);
            } else {
                throw new WrongCarRegistrationPlateException(
                        "Please enter valid letters at the end.");
            }
        }
    }

    private void ifEightDigits(String plate, int charRemoved) {

        //bottom
        if (plate.length() == 0) {
            return;
        }

        String currentLetter = String.valueOf(plate.charAt(0));
        String currentTownLetters = "";
        if (plate.length() >= 2){
            currentTownLetters = plate.substring(0, 2);
        }

        //check town letters
        if (charRemoved == 0) {
            if (bulgarianTownLetters.contains(currentTownLetters)) {
                charRemoved += 2;
                ifEightDigits(plate.substring(2), charRemoved);
                return;
            } else {
                throw new WrongCarRegistrationPlateException(
                        "Please enter valid town letters combination.");
            }
        }

        //check if 4 digits in the middle
        if (charRemoved > 1 && charRemoved < 6) {
            if (isDigit(currentLetter)) {
                charRemoved++;
                ifEightDigits(plate.substring(1), charRemoved);
                return;
            } else {
                throw new WrongCarRegistrationPlateException(
                        "Characters at position 3,4,5,6 must be digits.");
            }
        }

        //check last letters
        if (charRemoved > 5) {
            if (bulgarianAllLetters.contains(currentLetter)) {
                charRemoved++;
                ifEightDigits(plate.substring(1), charRemoved);
            } else {
                throw new WrongCarRegistrationPlateException(
                        "Please enter valid letters at the end.");
            }
        }
    }

    private Set<String> setBulgarianAllLetters() {

        Set<String> temp = new HashSet<>();
        temp.add("A");
        temp.add("B");
        temp.add("E");
        temp.add("K");
        temp.add("M");
        temp.add("H");
        temp.add("O");
        temp.add("P");
        temp.add("C");
        temp.add("T");
        temp.add("Y");
        temp.add("X");
        return temp;
    }

    private Set<String> setBulgarianTownLetters() {

        Set<String> temp = new HashSet<>();
        temp.add("E");
        temp.add("A");
        temp.add("B");
        temp.add("BT");
        temp.add("BH");
        temp.add("BP");
        temp.add("EB");
        temp.add("TX");
        temp.add("K");
        temp.add("KH");
        temp.add("OB");
        temp.add("M");
        temp.add("PA");
        temp.add("PK");
        temp.add("EH");
        temp.add("PB");
        temp.add("PP");
        temp.add("P");
        temp.add("CC");
        temp.add("CH");
        temp.add("CM");
        temp.add("CO");
        temp.add("C");
        temp.add("CA");
        temp.add("CB");
        temp.add("CT");
        temp.add("T");
        temp.add("X");
        temp.add("H");
        temp.add("Y");
        return temp;
    }

    private boolean isDigit(String string) {

        boolean temp = false;

        char digitToCheck = string.charAt(0);
        if (Character.isDigit(digitToCheck)) {
            temp = true;
        }

        return temp;
    }
}
