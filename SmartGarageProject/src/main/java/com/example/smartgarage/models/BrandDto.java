package com.example.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.example.smartgarage.models.constants.DtoValidationConstants.*;

public class BrandDto {

    public BrandDto() {
    }

    private int id;

    @NotNull
    @Size(max = 30, message = BRAND_NAME_LENGTH_MESSAGE)
    private String brandName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
