package com.example.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class PassRestartDto {

    @NotNull
    @Positive
    private int emailId;

    public PassRestartDto() {
    }

    public int getEmailId() {
        return emailId;
    }

    public void setEmailId(int emailId) {
        this.emailId = emailId;
    }
}
