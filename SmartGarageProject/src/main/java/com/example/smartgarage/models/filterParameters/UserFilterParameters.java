package com.example.smartgarage.models.filterParameters;

import java.util.Optional;

public class UserFilterParameters {

    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> email;
    private Optional<String> phone;
    private Optional<String> model;
    private Optional<String> make;
    private Optional<String> visitStart;
    private Optional<String> visitEnd;

    public UserFilterParameters() {
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public UserFilterParameters setFirstName(Optional<String> firstName) {
        this.firstName = firstName;
        return this;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public UserFilterParameters setLastName(Optional<String> lastName) {
        this.lastName = lastName;
        return this;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public UserFilterParameters setEmail(Optional<String> email) {
        this.email = email;
        return this;
    }

    public Optional<String> getPhone() {
        return phone;
    }

    public UserFilterParameters setPhone(Optional<String> phone) {
        this.phone = phone;
        return this;
    }

    public Optional<String> getModel() {
        return model;
    }

    public UserFilterParameters setModel(Optional<String> model) {
        this.model = model;
        return this;
    }

    public Optional<String> getMake() {
        return make;
    }

    public UserFilterParameters setMake(Optional<String> make) {
        this.make = make;
        return this;
    }

    public Optional<String> getVisitStart() {
        return visitStart;
    }

    public UserFilterParameters setVisitStart(Optional<String> visitStart) {
        this.visitStart = visitStart;
        return this;
    }

    public Optional<String> getVisitEnd() {
        return visitEnd;
    }

    public UserFilterParameters setVisitEnd(Optional<String> visitEnd) {
        this.visitEnd = visitEnd;
        return this;
    }
}
