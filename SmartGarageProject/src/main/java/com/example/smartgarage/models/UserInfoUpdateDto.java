package com.example.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.example.smartgarage.models.constants.DtoValidationConstants.*;

public class UserInfoUpdateDto {


    @Size(min = 2, max = 20, message = USER_NAME_LENGTH_MESSAGE)
    private String firstName;


    @Size(min = 2, max = 20, message = USER_NAME_LENGTH_MESSAGE)
    private String lastName;


    @Size(min = 10, max = 10, message = USER_PHONE_LENGTH_MESSAGE)
    private String phone;

    @NotNull
    @Size(min = 8, message = USER_PASSWORD_LENGTH_MESSAGE)
    private String password;

    public UserInfoUpdateDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
