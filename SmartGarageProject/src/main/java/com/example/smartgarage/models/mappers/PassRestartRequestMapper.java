package com.example.smartgarage.models.mappers;

import com.example.smartgarage.models.PassRestartRequest;
import com.example.smartgarage.models.PassRestartRequestDto;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class PassRestartRequestMapper {

    private final UserRepository userRepository;

    public PassRestartRequestMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public PassRestartRequest fromDto(PassRestartRequestDto passRestartRequestDto){

        User userRequesting = userRepository.getUserByEmail(passRestartRequestDto.getEmail());

        PassRestartRequest passRestartRequest = new PassRestartRequest();

        passRestartRequest.setUser(userRequesting);

        return passRestartRequest;
    }
}
