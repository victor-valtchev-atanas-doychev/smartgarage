package com.example.smartgarage.models.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * The string has to be a well-formed bulgarian civil plate with seven or eight signs.
 * For example: "B1234KK", "CA5678BB".
 * No special civil plates are covered ot any
 * government and service type of registration plates.
 *
 * @author Atanas Doychev
 * @author Victor Valtchev
 *
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PlateVerificationHelper.class)
public @interface CorrectLicensePlate {

    String message() default "Please enter valid registration plate.";
    Class<?>[] groups() default {};
    public abstract Class<? extends Payload>[] payload() default {};
}
