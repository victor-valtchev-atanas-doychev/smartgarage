package com.example.smartgarage.models.mappers;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserDto;
import com.example.smartgarage.models.UserInfoUpdateDto;
import com.example.smartgarage.models.filterParameters.CustomersSortParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;
import com.example.smartgarage.repositories.contracts.UserRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UserMapper {

    private final UserRepository userRepository;

    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public User fromDto(UserDto userDto){

        User user = new User();

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhone(userDto.getPhone());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        return user;
    }

    public User fromRegistrationDto(UserDto userDto){

        User user = new User();

        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhone(userDto.getPhone());
        user.setPassword(UUID.randomUUID().toString());
        return user;
    }

    public User fromUserInfoUpdateDto(UserInfoUpdateDto userInfoUpdateDto){

        User user = new User();

        user.setFirstName(userInfoUpdateDto.getFirstName());
        user.setLastName(userInfoUpdateDto.getLastName());
        user.setPhone(userInfoUpdateDto.getPhone());
        user.setPassword(userInfoUpdateDto.getPassword());
        return user;
    }

    public UserFilterParameters filterFromParametersList(Optional<String> firstName,
                                                         Optional<String> lastName,
                                                         Optional<String> email,
                                                         Optional<String> phone,
                                                         Optional<String> model,
                                                         Optional<String> make,
                                                         Optional<String> visitStart,
                                                         Optional<String> visitEnd){
        return new UserFilterParameters()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setEmail(email)
                .setPhone(phone)
                .setModel(model)
                .setMake(make)
                .setVisitStart(visitStart)
                .setVisitEnd(visitEnd);
    }

    public CustomersSortParameters sortFromParametersList(Optional<String> nameOrder,
                                                         Optional<String> dateOrder){
        return new CustomersSortParameters()
                .setNameOrder(nameOrder)
                .setDateOrder(dateOrder);
    }
}
