package com.example.smartgarage.models.mappers;


import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.models.VehicleDto;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import com.example.smartgarage.repositories.contracts.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class VehicleMapper {

    private final ModelRepository modelRepository;
    private final UserRepository userRepository;

    public VehicleMapper(ModelRepository modelRepository, UserRepository userRepository) {
        this.modelRepository = modelRepository;
        this.userRepository = userRepository;
    }

    public Vehicle fromDtoToObjectWhenCreateOrUpdate(VehicleDto vehicleDto){
        Vehicle vehicle = new Vehicle();
        vehicle.setId(vehicleDto.getId());
        vehicle.setLicensePlate(vehicleDto.getLicense_plate());
        vehicle.setVin(vehicleDto.getVin());
        vehicle.setYear(vehicleDto.getYear());
         vehicle.setOwner(userRepository.getById(vehicleDto.getOwner_id()));
        vehicle.setModel(modelRepository.getById(vehicleDto.getModel_id()));
        return vehicle;
    }
}
