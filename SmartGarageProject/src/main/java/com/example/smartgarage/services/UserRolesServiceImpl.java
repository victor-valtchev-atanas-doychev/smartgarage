package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.DuplicateEntityException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserRoles;
import com.example.smartgarage.repositories.contracts.UserRolesRepository;
import com.example.smartgarage.services.contracts.UserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;

@Service
public class UserRolesServiceImpl implements UserRolesService {

    private final UserRolesRepository userRolesRepository;

    @Autowired
    public UserRolesServiceImpl(UserRolesRepository userRolesRepository) {
        this.userRolesRepository = userRolesRepository;
    }

    @Override
    public List<UserRoles> getAll(User userToVerify) {
        verifyIfAdmin(userToVerify, ADMIN_OPERATION_ERROR_MESSAGE);
        return userRolesRepository.getAll();
    }

    @Override
    public void addRole(User userToVerify, UserRoles userRoles) {
        verifyIfAdmin(userToVerify, ADMIN_OPERATION_ERROR_MESSAGE);
        checkIfDuplicate(userRoles);
        userRolesRepository.addRole(userRoles);
    }

    @Override
    public void deleteRole(User userToVerify, UserRoles userRoles) {
        verifyIfAdmin(userToVerify, ADMIN_OPERATION_ERROR_MESSAGE);
        UserRoles userRolesToDelete = userRolesRepository.getByUserAndRole(userRoles);
        userRolesRepository.deleteRole(userRolesToDelete);
    }

    private void checkIfDuplicate(UserRoles userRoles){
        boolean duplicateExists = true;
        try {
            userRolesRepository.getByUserAndRole(userRoles);
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User-role relation",
                    "user " + userRoles.getUser().getFirstName(),
                    "and role " + userRoles.getRole().getRole());
        }
    }
}
