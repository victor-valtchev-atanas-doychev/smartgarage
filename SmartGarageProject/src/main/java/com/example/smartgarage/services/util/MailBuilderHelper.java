package com.example.smartgarage.services.util;

import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.User;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;

import static com.example.smartgarage.services.util.ServicesConstants.*;

@Component
public class MailBuilderHelper {

    public static String buildRegisterUserMailMessage(User userToCreate, String token){

        String activationLink = HTTP_LOCALHOST_8080_API_USERS_CUSTOMERS_CONFIRM + token;
        return String.format(
                USER_ACTIVATION_MAIL_MESSAGE,
                userToCreate.getFirstName(), userToCreate.getLastName(),
                activationLink,
                userToCreate.getPassword());
    }

    public static String buildPassResetMailMessage(User userToReset, String token) {

        String passResetLink = PASS_RESET_LINK + token;
        return String.format(USER_PASS_RESET_MESSAGE,
                userToReset.getFirstName(), userToReset.getLastName(),
                passResetLink);
    }

    public static String buildSingleOrderMailMessage(
            ServiceOrder serviceOrder, User userToSendTo, User employee){

        StringBuffer message = new StringBuffer();

        message.append(buildHelloText(userToSendTo));

        message.append(buildVehicleText(serviceOrder));

        message.append(buildSingleOrderText(serviceOrder));

        message.append(buildEmployeeMailSignature(employee));

        message.append(buildThanksText());

        return message.toString();
    }

    public static String buildFullVisitMailMessage(
            List<ServiceOrder> serviceOrders, User userToSendTo, User employee){

        StringBuffer message = new StringBuffer();

        message.append(buildHelloText(userToSendTo));

        message.append(buildVehicleText(serviceOrders.get(0)));

        message.append(buildMultiOrderText(serviceOrders));

        message.append(buildEmployeeMailSignature(employee));

        message.append(buildThanksText());

        return message.toString();

    }

    public static String buildFullVisitMailMessageAutoByCustomer(
            List<ServiceOrder> serviceOrders, User userToSendTo){

        StringBuffer message = new StringBuffer();

        message.append(buildHelloText(userToSendTo));

        message.append(buildVehicleText(serviceOrders.get(0)));

        message.append(buildMultiOrderText(serviceOrders));

        message.append(buildThanksText());

        return message.toString();

    }

    private static String buildMultiOrderText(List<ServiceOrder> serviceOrders){

        StringBuffer message = new StringBuffer();
        double totalPrice = 0.0;

        for (ServiceOrder singleOrder : serviceOrders){
            message.append(buildSingleOrderText(singleOrder));
            totalPrice += singleOrder.getService().getPrice();
        }

        message.append(String.format("Total Price: %sEUR%n%n", totalPrice));

        return message.toString();
    }

    private static String buildSingleOrderText(ServiceOrder serviceOrder){

        String pattern = "dd-MM-yyyy";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

        return String.format(
                SINGLE_SERVICE_ORDER_MAIL_MESSAGE,
                serviceOrder.getId(),
                serviceOrder.getEndDate().format(formatter),
                serviceOrder.getService().getName(),
                serviceOrder.getService().getPrice());
    }

    private static String buildVehicleText(ServiceOrder serviceOrder){

        return String.format(
                VEHICLE_INFO_TEXT,
                serviceOrder.getVisit().getVehicle().getModel().getBrand().getBrandName(),
                serviceOrder.getVisit().getVehicle().getModel().getModelName(),
                serviceOrder.getVisit().getVehicle().getLicensePlate().toUpperCase());
    }

    private static String buildHelloText(User userToSendTo){
        return String.format(HELLO_CUSTOMER_MESSAGE,
                userToSendTo.getFirstName(), userToSendTo.getLastName());
    }

    private static String buildEmployeeMailSignature(User employee){

        return String.format(
                EMPLOYEE_MAIL_SIGNATURE,
                employee.getFirstName(), employee.getLastName(),
                employee.getEmail(), employee.getPhone());
    }

    private static String buildThanksText(){
        return THANKS_MESSAGE;
    }
}
