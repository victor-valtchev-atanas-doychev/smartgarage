package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserRoles;

import java.util.List;

public interface UserRolesService {

    List<UserRoles> getAll(User user);

    void addRole(User user, UserRoles userRoles);

    void deleteRole(User user, UserRoles userRoles);
}
