package com.example.smartgarage.services.contracts;

public interface EmailService {

    void send(String to, String subject, String text);
}
