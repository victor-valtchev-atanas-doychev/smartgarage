package com.example.smartgarage.services;

import com.example.smartgarage.models.Model;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import com.example.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Model> getAll(User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return modelRepository.getAll();
    }

    @Override
    public Model getById(int id, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return modelRepository.getById(id);
    }

    @Override
    public void create(Model model, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        modelRepository.create(model);
    }

    @Override
    public void update(Model model, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        modelRepository.getById(model.getId());
        modelRepository.update(model);
    }

    @Override
    public void delete(int id, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        modelRepository.getById(id);
        Model model = new Model();
        model.setId(id);
        modelRepository.delete(model);
    }
}
