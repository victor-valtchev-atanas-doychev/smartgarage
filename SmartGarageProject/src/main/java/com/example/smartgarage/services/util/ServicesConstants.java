package com.example.smartgarage.services.util;

public class ServicesConstants {

    public static final int HOURS_1 = 1;

    public static final int HOURS_24 = 24;

    public static final String PASS_RESET = "Password reset";

    public static final String SERVICE_ORDER = "Service order";

    public static final String FULL_VISIT_REPORT = "Full Visit Report";

    public static final String PROFILE_ACTIVATION = "Profile activation";

    public static final String LINK_EXPIRED_MESSAGE = "Link has expired.";

    public static final String SMART_GARAGE_APP_GMAIL_COM = "smart.garage.app@gmail.com";

    public static final String LINK_ALREADY_USED = "This link has been used already.";

    public static final String UNAUTHORIZED_OPERATION = "Unauthorized operation.";

    public static final String HELLO_CUSTOMER_MESSAGE =
            "Hello, %s %s, here is your service order information:%n%n";

    public static final String VISIT_NOT_YOURS = "This visit is not yours.";

    public static final String HTTP_LOCALHOST_8080_API_USERS_CUSTOMERS_CONFIRM =
            "http://localhost:8080/api/users/customers/confirm?token=";

    public static final String PASS_RESET_LINK =
            "http://localhost:8080/user-pass-restart?token=";

    public static final String SINGLE_SERVICE_ORDER_MAIL_MESSAGE =
                    "Service order No: %d%n" +
                    "Due to: %s %n" +
                    "Type of service: %s %n" +
                    "Price: %s %n%n";

    public static final String VEHICLE_INFO_TEXT =
            "Vehicle: %s, %s%n" +
            "With registration: %s %n%n";

    public static final String EMPLOYEE_MAIL_SIGNATURE =
            "This order was build by %s %s.%n" +
            "If you have any questions please contact me.%n" +
            "Email: %s%n" +
            "Phone: %s%n%n";

    public static final String USER_ACTIVATION_MAIL_MESSAGE =
            "Hello, %s %s, please activate your account within 24 hours in the following link.%n%n" +
            "%s%n%n" +
            "Then you can login with your email and provided password. %n%n" +
            "Password:%s";

    public static final String USER_PASS_RESET_MESSAGE =
            "Hello, %s %s, you can reset your password within 1 hours in the following link.%n%n" +
                    "%s%n%n";

    public static final String THANKS_MESSAGE =
            "Thank you for using Smart Garage.";

    public static final String PASSWORDS_DO_NOT_MATCH =
            "Passwords don't match!";

    public static final String NEW_PASSWORD_CANNOT_BE_THE_SAME_AS_THE_OLD =
            "New password cannot be the same as the old.";

    public static final String UNAUTHORIZED_PASSWORD_CHANGE =
            "Unauthorized password change.";

    public static final String START_END_DATE_SHOULD_PROVIDED =
            "Start date and/or end date should be provided.";

}
