package com.example.smartgarage.services;

import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ServiceOrderFilterParameters;
import com.example.smartgarage.repositories.contracts.ServiceOrderRepository;
import com.example.smartgarage.repositories.util.RepositoryHelper;
import com.example.smartgarage.services.contracts.EmailService;
import com.example.smartgarage.services.contracts.ServiceOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;
import static com.example.smartgarage.services.util.MailBuilderHelper.*;
import static com.example.smartgarage.services.util.ServicesConstants.*;

@Service
public class ServiceOrderServiceImpl implements ServiceOrderService {

    private final ServiceOrderRepository serviceOrderRepository;
    private final EmailService emailService;

    @Autowired
    public ServiceOrderServiceImpl(ServiceOrderRepository serviceOrderRepository,
                                   EmailService emailService) {
        this.serviceOrderRepository = serviceOrderRepository;
        this.emailService = emailService;
    }

    @Override
    public List<ServiceOrder> getAll(User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return serviceOrderRepository.getAll();
    }

    @Override
    public ServiceOrder getById(int id, User user) {
        //to be given access to Owner as well.
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return serviceOrderRepository.getById(id);
    }


    @Override
    public void create(ServiceOrder serviceOrder, User employee) {
        verifyIfEmployeeOrAdmin(employee, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        serviceOrderRepository.create(serviceOrder);
    }

    @Transactional
    @Override
    public void createAndSendMail(ServiceOrder serviceOrder, User employee) {
        verifyIfEmployeeOrAdmin(employee, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);

        serviceOrderRepository.create(serviceOrder);

        User userToSentMail = serviceOrder.getVisit().getVehicle().getOwner();
        String message = buildSingleOrderMailMessage(serviceOrder, userToSentMail, employee);

        emailService.send(userToSentMail.getEmail(), SERVICE_ORDER, message);
    }

    @Override
    public void update(ServiceOrder serviceOrder, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        serviceOrderRepository.getById(serviceOrder.getId());
        serviceOrderRepository.update(serviceOrder);
    }

    @Override
    public void delete(int id, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        ServiceOrder serviceOrder = serviceOrderRepository.getById(id);
        serviceOrderRepository.delete(serviceOrder);
    }

    @Override
    public List<ServiceOrder> getAllServicesForMonth(String year, String month, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        LocalDateTime currentMonth = RepositoryHelper.concatenateYearAndMonthFromFrontEnd(year, month);
        LocalDateTime nextMonth = currentMonth.plusMonths(1);
        return serviceOrderRepository.getAllServicesForMonth(currentMonth, nextMonth);
    }

    @Override
    public List<ServiceOrder> getAllServicesByVisitId(Integer visitId, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return serviceOrderRepository.getAllServicesByVisitId(visitId);
    }

    @Override
    public void createFromFrontEnd(ServiceOrder serviceOrder, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        serviceOrderRepository.create(serviceOrder);
    }

    @Override
    public List<ServiceOrder> getAllServicesByUserId(Integer userId, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return serviceOrderRepository.getAllServicesByUserId(userId);
    }

    @Override
    public List<ServiceOrder> filterByVehicleIdAndOrStartDate(User user, ServiceOrderFilterParameters sofp) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return serviceOrderRepository.filterByVehicleIdAndOrStartDate(sofp);
    }
}
