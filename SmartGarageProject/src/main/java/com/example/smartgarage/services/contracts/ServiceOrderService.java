package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ServiceOrderFilterParameters;

import java.util.List;

public interface ServiceOrderService extends CrudService<ServiceOrder>{

    List<ServiceOrder> getAllServicesForMonth(String year, String month, User user);

    List<ServiceOrder> getAllServicesByVisitId(Integer visitId, User user);

    void createAndSendMail(ServiceOrder serviceOrder, User employee);

    void createFromFrontEnd(ServiceOrder serviceOrder, User user);

    List<ServiceOrder> getAllServicesByUserId(Integer userId, User user);

    List<ServiceOrder> filterByVehicleIdAndOrStartDate(User user, ServiceOrderFilterParameters sofp);
}
