package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.DuplicateEntityException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.repositories.contracts.VehicleRepository;
import com.example.smartgarage.repositories.util.RepositoryHelper;
import com.example.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Vehicle> getAll(User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return vehicleRepository.getAll();
    }

    @Override
    public Vehicle getById(int id, User user) {
        //to be given answer to Owner as well.
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return vehicleRepository.getById(id);
    }

    @Override
    public void create(Vehicle vehicle, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        checkIfVinDuplicate(vehicle);
        vehicleRepository.create(vehicle);
    }

    @Override
    public void update(Vehicle vehicle, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        vehicleRepository.getById(vehicle.getId());
        checkIfVinDuplicate(vehicle);
        vehicleRepository.update(vehicle);
    }

    @Override
    public void delete(int id, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        vehicleRepository.getById(id);
        Vehicle vehicle = new Vehicle();
        vehicle.setId(id);
        vehicleRepository.delete(vehicle);
    }

    @Override
    public List<Vehicle> getByOwner(Integer ownerId, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return vehicleRepository.getByOwnerId(ownerId);
    }

    @Override
    public List<Vehicle> getAllVehiclesByMonth(String year, String month, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        LocalDateTime currentMonth = RepositoryHelper.concatenateYearAndMonthFromFrontEnd(year, month);
        LocalDateTime nextMonth = currentMonth.plusMonths(1);
        return vehicleRepository.getAllVehiclesByMonth(currentMonth, nextMonth);
    }

    private void checkIfVinDuplicate(Vehicle vehicle){
        boolean duplicateExists = true;

        try {
            vehicleRepository.getByVin(vehicle.getVin());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle",
                    "VIN:" + vehicle.getVin());
        }
    }
}
