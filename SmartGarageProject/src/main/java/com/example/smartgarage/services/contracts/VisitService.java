package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Visit;

import java.util.List;
import java.util.Optional;

public interface VisitService extends CrudService<Visit>{

    List<Visit> getVisitByStartAndEndDate(Optional<String> startDate, Optional<String> endDate, User user);

    List<Visit> getAllVisitsByMonth(String year, String month, User user);

    List<Visit> getAllVisitsByVehicleId(String vehicleId, User user);

    void sendMailOfAllOrdersForAVisit(Integer visitId, User userToConfirm);

    List<Visit> getAllActiveVisits(User user);
}
