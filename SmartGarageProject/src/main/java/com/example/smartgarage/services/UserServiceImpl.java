package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.DuplicateEntityException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.exceptions.UnauthorizedOperationException;
import com.example.smartgarage.models.ConfirmationToken;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserInfoUpdateDto;
import com.example.smartgarage.models.filterParameters.CustomersSortParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;
import com.example.smartgarage.repositories.contracts.UserRepository;
import com.example.smartgarage.services.contracts.ConfirmationTokenService;
import com.example.smartgarage.services.contracts.EmailService;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;
import static com.example.smartgarage.services.util.MailBuilderHelper.*;
import static com.example.smartgarage.services.util.ServicesConstants.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ConfirmationTokenService confirmationTokenService;
    private final EmailService emailService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           ConfirmationTokenService confirmationTokenService,
                           EmailService emailService) {
        this.userRepository = userRepository;
        this.confirmationTokenService = confirmationTokenService;
        this.emailService = emailService;
    }

    @Override
    public List<User> getAll(User userToVerify) {
        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return userRepository.getAll();
    }

    @Override
    public List<User> getAllCustomersWithSort(User userToVerify, CustomersSortParameters csp) {
        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return userRepository.getAllCustomersWithSort(csp);
    }

    @Override
    public User getById(int id, User userToVerify) {
        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return userRepository.getById(id);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public List<User> filter(User userToVerify, UserFilterParameters ufp) {
        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return userRepository.filter(ufp);
    }

    @Override
    public void create(User userToCreate, User userToVerify) {
        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        checkIfDuplicateUserExists(userToCreate);
        userRepository.create(userToCreate);
    }

    @Override
    public void update(User userToUpdate, User userToVerify) {
        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        checkIfDuplicateUserExists(userToUpdate);
        userRepository.update(userToUpdate);
    }

    @Override
    public void updateByUser(User userToVerify, User userToUpdate) {

        if (userToUpdate.getId() != userToVerify.getId()){
            throw new UnauthorizedOperationException(UNAUTHORIZED_OPERATION);
        }

        if (!userToUpdate.getPassword().equals(userToVerify.getPassword())){
            throw new UnauthorizedOperationException(UNAUTHORIZED_OPERATION);
        }

        userRepository.update(userToUpdate);
    }

    @Override
    public void delete(int id, User userToVerify) {
        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        User userToDelete = userRepository.getById(id);
        userRepository.delete(userToDelete);
    }

    @Override
    public String registerUser(User userToCreate, User userToVerify) {

        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        checkIfDuplicateUserExists(userToCreate);

        userRepository.create(userToCreate);

        String token = confirmationTokenService.create(userToCreate, HOURS_24);
        String message = buildRegisterUserMailMessage(userToCreate, token);

        emailService.send(userToCreate.getEmail(), PROFILE_ACTIVATION, message);
        return token;
    }

    @Transactional
    @Override
    public void confirmUser(String token) {

        ConfirmationToken confirmationToken = confirmationTokenService.getToken(token);

        confirmationTokenService.checkIfTokenActive(confirmationToken);

        //enable user account
        User userToBeEnabled = userRepository.getById(confirmationToken.getUser().getId());
        userToBeEnabled.setEnabled(true);
        userRepository.update(userToBeEnabled);

        //deactivate and update token
        confirmationToken.setConfirmedAt(LocalDateTime.now());
        confirmationTokenService.setConfirmedAt(confirmationToken);

    }


    private void checkIfDuplicateUserExists(User user){

        //check if duplicate mail
        boolean duplicateEmailExists = true;
        try {
            User existingUser = userRepository.getUserByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException(user.getEmail());
        }

        //check if duplicate phone
        boolean duplicatePhoneExists = true;
        try{
            User existingUser2 = userRepository.getUserByPhone(user.getPhone());
        } catch (EntityNotFoundException e) {
            duplicatePhoneExists = false;
        }

        if (duplicatePhoneExists) {
            throw new DuplicateEntityException(user.getPhone());
        }
    }
}
