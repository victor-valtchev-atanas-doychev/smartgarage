package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.ConfirmationToken;
import com.example.smartgarage.models.User;

public interface ConfirmationTokenService {

    String create(User userToCreate, int hoursActive);

    ConfirmationToken getToken(String token);

    void setConfirmedAt(ConfirmationToken token);

    void checkIfTokenActive(ConfirmationToken tokenToCheck);
}
