package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.OrderOfOperationsException;
import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.repositories.contracts.ServiceOrderRepository;
import com.example.smartgarage.repositories.contracts.VisitRepository;
import com.example.smartgarage.repositories.util.RepositoryHelper;
import com.example.smartgarage.services.contracts.EmailService;
import com.example.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;
import static com.example.smartgarage.services.util.MailBuilderHelper.buildFullVisitMailMessage;
import static com.example.smartgarage.services.util.MailBuilderHelper.buildFullVisitMailMessageAutoByCustomer;
import static com.example.smartgarage.services.util.ServicesConstants.*;

@Service
public class VisitServiceImpl implements VisitService {

    private final VisitRepository visitRepository;
    private final ServiceOrderRepository serviceOrderRepository;
    private final EmailService emailService;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository,
                            ServiceOrderRepository serviceOrderRepository,
                            EmailService emailService) {
        this.visitRepository = visitRepository;
        this.serviceOrderRepository = serviceOrderRepository;
        this.emailService = emailService;
    }

    @Override
    public List<Visit> getAll(User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return visitRepository.getAll();
    }

    @Override
    public Visit getById(int id, User user) {
        // to be given access to Owner as well.
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return visitRepository.getById(id);
    }

    @Override
    public void create(Visit visit, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        visitRepository.create(visit);
    }

    @Override
    public void update(Visit visit, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        visitRepository.getById(visit.getId());
        visitRepository.update(visit);
    }

    @Override
    public void delete(int id, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        Visit visit = visitRepository.getById(id);
        visitRepository.delete(visit);
    }

    @Override
    public List<Visit> getVisitByStartAndEndDate(Optional<String> startDate,
                                                 Optional<String> endDate, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        if (startDate.isEmpty() && endDate.isEmpty()) {
            throw new OrderOfOperationsException(START_END_DATE_SHOULD_PROVIDED);
        } else {
            return visitRepository.getVisitByStartAndEndDate(startDate, endDate);
        }
    }

    @Override
    public List<Visit> getAllVisitsByMonth(String year, String month, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        LocalDateTime currentMonth = RepositoryHelper.concatenateYearAndMonthFromFrontEnd(year, month);
        LocalDateTime nextMonth = currentMonth.plusMonths(1);
        return visitRepository.getAllVisitsByMonth(currentMonth, nextMonth);
    }

    @Override
    public List<Visit> getAllVisitsByVehicleId(String vehicleId, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return visitRepository.getAllVisitsByVehicleId(vehicleId);
    }

    @Override
    public List<Visit> getAllActiveVisits(User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return visitRepository.getAllActiveVisits();
    }

    @Override
    public void sendMailOfAllOrdersForAVisit(Integer visitId, User userToConfirm){
        Visit visit = visitRepository.getById(visitId);
        User userToSendMailTo = visit.getVehicle().getOwner();

        verifyIfUserOwnsVisitOrEmployeeOrAdmin(userToConfirm, visit, VISIT_NOT_YOURS);

        List<ServiceOrder> allOrders = serviceOrderRepository.getAllServicesByVisitId(visitId);

        String message = "";

        if (userToConfirm.getId() == userToSendMailTo.getId()){
            message = buildFullVisitMailMessageAutoByCustomer(allOrders, userToSendMailTo);
        }else {
            message = buildFullVisitMailMessage(allOrders, userToSendMailTo, userToConfirm);
        }


        emailService.send(userToSendMailTo.getEmail(), FULL_VISIT_REPORT, message);
    }
}
