package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Brand;

public interface BrandService extends CrudService<Brand>{
}
