package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.ShopService;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ShopServiceFilterParameters;

import java.util.List;
import java.util.Optional;

public interface ShopServiceService extends CrudService<ShopService>{
    List<ShopService> getShopServicesByNameAndPrice(ShopServiceFilterParameters ssfs, User user);

    List<ShopService> getAllServicesLinkedToACustomer(User user, ShopServiceFilterParameters ssfs);
}
