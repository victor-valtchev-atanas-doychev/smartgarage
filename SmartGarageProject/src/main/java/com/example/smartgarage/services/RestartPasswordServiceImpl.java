package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthenticationFailureException;
import com.example.smartgarage.exceptions.OrderOfOperationsException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.repositories.contracts.RestartPasswordRepository;
import com.example.smartgarage.repositories.contracts.UserRepository;
import com.example.smartgarage.services.contracts.ConfirmationTokenService;
import com.example.smartgarage.services.contracts.EmailService;
import com.example.smartgarage.services.contracts.RestartPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;
import static com.example.smartgarage.services.util.MailBuilderHelper.*;
import static com.example.smartgarage.services.util.ServicesConstants.*;

@Service
public class RestartPasswordServiceImpl implements RestartPasswordService {

    private final EmailService emailService;
    private final UserRepository userRepository;
    private final ConfirmationTokenService confirmationTokenService;
    private final RestartPasswordRepository restartPasswordRepository;

    @Autowired
    public RestartPasswordServiceImpl(EmailService emailService, UserRepository userRepository,
                                      ConfirmationTokenService confirmationTokenService,
                                      RestartPasswordRepository restartPasswordRepository) {
        this.emailService = emailService;
        this.userRepository = userRepository;
        this.confirmationTokenService = confirmationTokenService;
        this.restartPasswordRepository = restartPasswordRepository;
    }

    @Override
    public void createRestartPasswordRequest(PassRestartRequest passRestartRequest) {

        restartPasswordRepository.createRestartPasswordRequest(passRestartRequest);

    }

    @Override
    public void sendResetPasswordMail(User userToVerify, PassRestartDto passRestartDto){

        verifyIfEmployeeOrAdmin(userToVerify, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        User userToReset = userRepository.getUserById(passRestartDto.getEmailId());

        String token = confirmationTokenService.create(userToReset, HOURS_1);
        String message = buildPassResetMailMessage(userToReset, token);

        emailService.send(userToReset.getEmail(), PASS_RESET, message);
    }

    @Override
    public void sendResetPasswordMailAutomatically(String email){

        User userToReset = userRepository.getUserByEmail(email);
        String token = confirmationTokenService.create(userToReset, HOURS_1);
        String message = buildPassResetMailMessage(userToReset, token);

        emailService.send(email, PASS_RESET, message);
    }


    @Transactional
    @Override
    public void resetUserPassword(ConfirmationToken confirmationToken,
                                  PassRestartFromUserDto passRestartFromUserDto){

        confirmationTokenService.checkIfTokenActive(confirmationToken);

        if (!passRestartFromUserDto.getPassword().equals(passRestartFromUserDto.getRepeatPassword())){
            throw new OrderOfOperationsException(PASSWORDS_DO_NOT_MATCH);
        }

        User userToUpdate = userRepository.getById(confirmationToken.getUser().getId());

        //set NEW password
        userToUpdate.setPassword(passRestartFromUserDto.getPassword());

        //update user
        userRepository.update(userToUpdate);

        //deactivate and update token
        confirmationToken.setConfirmedAt(LocalDateTime.now());
        confirmationTokenService.setConfirmedAt(confirmationToken);

        //update request restart date
        //todo there is bug here, we must get by userId,
        // and in DB request must be connected to concrete token
        PassRestartRequest requestToUpdate =
                restartPasswordRepository.getByUser(confirmationToken.getUser());
        requestToUpdate.setRestartDate(LocalDateTime.now());
        restartPasswordRepository.updateRequest(requestToUpdate);
    }

    @Override
    public void changeUserPassword(User userToConfirmAndChange, PassChangeFromUserDto passChangeFromUserDto){

        //check if user who who logged is changing his password
        if (!userToConfirmAndChange.getPassword().equals(passChangeFromUserDto.getOldPassword())){
            throw new AuthenticationFailureException(UNAUTHORIZED_PASSWORD_CHANGE);
        }

        if (passChangeFromUserDto.getOldPassword().equals(passChangeFromUserDto.getNewPassword())) {
            throw new OrderOfOperationsException(NEW_PASSWORD_CANNOT_BE_THE_SAME_AS_THE_OLD);
        }

        if (!passChangeFromUserDto.getNewPassword().equals(passChangeFromUserDto.getRepeatNewPassword())) {
            throw new OrderOfOperationsException(PASSWORDS_DO_NOT_MATCH);
        }

        User userToUpdate = userRepository.getById(userToConfirmAndChange.getId());

        //update password
        userToUpdate.setPassword(passChangeFromUserDto.getNewPassword());

        userRepository.update(userToUpdate);

    }

    @Override
    public List<PassRestartRequest> getUncompletedRequests (User userToVerify){

        verifyIfEmployeeOrAdmin(userToVerify,EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);

        return restartPasswordRepository.getUncompletedRequests();
    }

    @Override
    public List<PassRestartRequest> getAllPassRestartRequest(User user) {
        verifyIfEmployeeOrAdmin(user,EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return  restartPasswordRepository.getAllPassRestartRequest();
    }
}
