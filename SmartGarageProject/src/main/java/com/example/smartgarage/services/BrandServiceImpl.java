package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.DuplicateEntityException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Brand;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.BrandRepository;
import com.example.smartgarage.services.contracts.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;

@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;

    @Autowired
    public BrandServiceImpl(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Override
    public List<Brand> getAll(User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return brandRepository.getAll();
    }

    @Override
    public Brand getById(int id, User user) {
        // to be given Access to owner.
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return brandRepository.getById(id);
    }

    @Override
    public void create(Brand brand, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        checkIfDuplicate(brand);
        brandRepository.create(brand);
    }

    @Override
    public void update(Brand brand, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        brandRepository.getById(brand.getId());
        checkIfDuplicate(brand);
        brandRepository.update(brand);
    }

    @Override
    public void delete(int id, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        Brand brand = brandRepository.getById(id);
        brandRepository.delete(brand);
    }

    private void checkIfDuplicate(Brand brand){
        boolean duplicateExists = true;

        try {
            brandRepository.getByName(brand.getBrandName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Brand",
                    "name " + brand.getBrandName());
        }
    }
}
