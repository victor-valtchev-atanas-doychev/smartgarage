package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserInfoUpdateDto;
import com.example.smartgarage.models.filterParameters.CustomersSortParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;

import java.util.List;

public interface UserService extends CrudService<User>{

    List<User> getAllCustomersWithSort(User user, CustomersSortParameters csp);

    User getUserByEmail(String email);

    List<User> filter(User userToVerify, UserFilterParameters ufp);

    String registerUser(User userToCreate, User userToVerify);

    void confirmUser(String token);

    void updateByUser(User userToVerify, User userToUpdate);
}
