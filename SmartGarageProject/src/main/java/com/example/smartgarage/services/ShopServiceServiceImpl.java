package com.example.smartgarage.services;

import com.example.smartgarage.models.ShopService;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ShopServiceFilterParameters;
import com.example.smartgarage.repositories.contracts.ShopServiceRepository;
import com.example.smartgarage.services.contracts.ShopServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.smartgarage.services.util.AuthorizationHelper.*;

@Service
public class ShopServiceServiceImpl implements ShopServiceService {

    private final ShopServiceRepository shopServiceRepository;

    @Autowired
    public ShopServiceServiceImpl(ShopServiceRepository shopServiceRepository) {
        this.shopServiceRepository = shopServiceRepository;
    }

    @Override
    public List<ShopService> getAll(User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return shopServiceRepository.getAll();
    }

    @Override
    public ShopService getById(int id, User user) {
        // to be given access to owner as well.
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return shopServiceRepository.getById(id);
    }

    @Override
    public void create(ShopService shopService, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        shopServiceRepository.create(shopService);
    }

    @Override
    public void update(ShopService shopService, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        shopServiceRepository.getById(shopService.getId());
        shopServiceRepository.update(shopService);
    }

    @Override
    public void delete(int id, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        ShopService shopService = shopServiceRepository.getById(id);
        shopServiceRepository.delete(shopService);
    }

    @Override
    public List<ShopService> getShopServicesByNameAndPrice(
            ShopServiceFilterParameters ssfs, User user) {
        verifyIfEmployeeOrAdmin(user, EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE);
        return shopServiceRepository.getShopServicesByNameAndPrice(ssfs);
    }

    @Override
    public List<ShopService> getAllServicesLinkedToACustomer(
            User user, ShopServiceFilterParameters ssfs) {
        return shopServiceRepository.getAllServicesLinkedToACustomer(user, ssfs);
    }
}
