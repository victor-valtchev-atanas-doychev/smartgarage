package com.example.smartgarage.services;

import com.example.smartgarage.models.ConfirmationToken;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.ConfirmationTokenRepository;
import com.example.smartgarage.repositories.contracts.UserRepository;
import com.example.smartgarage.services.contracts.ConfirmationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

import static com.example.smartgarage.services.util.ServicesConstants.LINK_ALREADY_USED;
import static com.example.smartgarage.services.util.ServicesConstants.LINK_EXPIRED_MESSAGE;


@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    private final ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    public ConfirmationTokenServiceImpl(ConfirmationTokenRepository confirmationTokenRepository) {
        this.confirmationTokenRepository = confirmationTokenRepository;
    }

    @Override
    public String create(User userToCreate, int hoursActive) {

        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken =
                new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusHours(hoursActive),
                userToCreate);

        confirmationTokenRepository.create(confirmationToken);
        return token;
    }

    @Override
    public ConfirmationToken getToken(String token) {
        return confirmationTokenRepository.getToken(token);
    }

    @Override
    public void setConfirmedAt(ConfirmationToken token) {
        confirmationTokenRepository.setConfirmedAt(token);
    }

    @Override
    public void checkIfTokenActive(ConfirmationToken tokenToCheck){

        if (tokenToCheck.getConfirmedAt() != null) {
            throw new IllegalStateException(LINK_ALREADY_USED);
        }

        LocalDateTime expiresAt = tokenToCheck.getExpiresAt();
        if (expiresAt.isBefore(LocalDateTime.now())) {
            throw new IllegalStateException(LINK_EXPIRED_MESSAGE);
        }
    }
}
