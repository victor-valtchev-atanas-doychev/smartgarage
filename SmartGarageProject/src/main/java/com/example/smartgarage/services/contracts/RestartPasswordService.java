package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.*;

import java.util.List;

public interface RestartPasswordService {

    void createRestartPasswordRequest(PassRestartRequest passRestartRequest);

    void sendResetPasswordMail(User userToVerify, PassRestartDto passRestartDto);

    void resetUserPassword(ConfirmationToken confirmationToken,
                           PassRestartFromUserDto passRestartFromUserDto);

    void changeUserPassword(User userToConfirmAndChange,
                            PassChangeFromUserDto passChangeFromUserDto);

    List<PassRestartRequest> getUncompletedRequests (User userToVerify);

    List<PassRestartRequest> getAllPassRestartRequest(User user);

    void sendResetPasswordMailAutomatically(String email);
}
