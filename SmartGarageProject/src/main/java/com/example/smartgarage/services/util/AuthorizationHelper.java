package com.example.smartgarage.services.util;

import com.example.smartgarage.exceptions.UnauthorizedOperationException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Visit;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationHelper {

    public static final String EMPLOYEE_ADMIN_OPERATION_ERROR_MESSAGE =
            "Only employees or admins have access to this operation";

    public static final String ADMIN_OPERATION_ERROR_MESSAGE =
            "Only admins have access to this operation";

    public static void verifyIfEmployee(User user, String message) {
        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException(message);
        }
    }

    public static void verifyIfCustomer(User user, String message) {
        if (!user.isCustomer()) {
            throw new UnauthorizedOperationException(message);
        }
    }

    public static void verifyIfAdmin(User user, String message) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(message);
        }
    }

    public static void verifyIfEmployeeOrAdmin(User user, String message) {
        if (!user.isEmployee() && !user.isAdmin()) {
            throw new UnauthorizedOperationException(message);
        }
    }

    public static void verifyIfUserOwnsVisitOrEmployeeOrAdmin(User userToConfirm,
                                                              Visit visit,
                                                              String message) {

        if ((userToConfirm.getId() != visit.getVehicle().getOwner().getId()) &&
                (!userToConfirm.isEmployee() && !userToConfirm.isAdmin())) {
            throw new UnauthorizedOperationException(message);
        }
    }
}
