package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;

import java.util.List;

public interface CrudService<T> {

    List<T> getAll(User user);

    T getById(int id, User user);

    void create(T object, User user);

    void update(T object, User user);

    void delete(int id, User User);
}
