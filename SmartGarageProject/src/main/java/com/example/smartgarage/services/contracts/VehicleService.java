package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.Vehicle;

import java.util.List;

public interface VehicleService extends CrudService<Vehicle>{

    List<Vehicle> getByOwner(Integer integer, User user);

    List<Vehicle> getAllVehiclesByMonth(String year, String month, User user);
}
