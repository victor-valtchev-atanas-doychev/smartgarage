package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.PassRestartRequest;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.RestartPasswordRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;

@Repository
public class RestartPasswordRepositoryImpl implements RestartPasswordRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RestartPasswordRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createRestartPasswordRequest(PassRestartRequest passRestartRequest) {
        try (Session session = sessionFactory.openSession()) {
            session.save(passRestartRequest);
        }
    }

    @Override
    public PassRestartRequest getByUser(User user){
        try (Session session = sessionFactory.openSession()) {
            Query<PassRestartRequest> query = session.createQuery(
                    "from PassRestartRequest where user = :user", PassRestartRequest.class);
            query.setParameter("user", user);
            List<PassRestartRequest> list = query.list();
            return list.get(0);
        } catch (Exception e) {
            throw new EntityNotFoundException(PASS_REQUEST_NOT_EXIST);
        }
    }

    @Override
    public List<PassRestartRequest> getUncompletedRequests(){
        try (Session session = sessionFactory.openSession()) {

            Query<PassRestartRequest> query = session.createQuery(
                    "from PassRestartRequest where restartDate is null", PassRestartRequest.class);

            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_UNCOMPLETED_PASS_REQUESTS);
            }

            return query.list();
        }
    }

    @Override
    public List<PassRestartRequest> getAllPassRestartRequest() {
        try (Session session = sessionFactory.openSession()) {
            Query<PassRestartRequest> query = session.createQuery(
                    "from PassRestartRequest", PassRestartRequest.class);

            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_PASS_RESTART_REQUESTS);
            }
            return query.list();
        }
    }

    @Override
    public void updateRequest(PassRestartRequest request){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(request);
            session.getTransaction().commit();
        }
    }
}
