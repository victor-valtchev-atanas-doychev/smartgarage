package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.UserRoles;

import java.util.List;

public interface UserRolesRepository {

    List<UserRoles> getAll();

    void addRole(UserRoles userRoles);

    void deleteRole(UserRoles userRoles);

    UserRoles getByUserAndRole(UserRoles userRoles);
}
