package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.UserRoles;
import com.example.smartgarage.repositories.contracts.UserRolesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;

@Repository
public class UserRolesRepositoryImpl implements UserRolesRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRolesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<UserRoles> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserRoles> query = session.createQuery(
                    "from UserRoles order by userId.id asc", UserRoles.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_USER_ROLES);
            }
            return query.list();
        }
    }

    @Override
    public void addRole(UserRoles userRoles) {
        try (Session session = sessionFactory.openSession()) {
            session.save(userRoles);
        }
    }

    @Override
    public void deleteRole(UserRoles userRoles) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(userRoles);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserRoles getByUserAndRole(UserRoles userRoles) {
        try(Session session = sessionFactory.openSession()){
            Query<UserRoles> query = session.createQuery(
                    "from UserRoles where userId.id = :userId " +
                            "and roleId.id = :roleId", UserRoles.class);
            query.setParameter("userId", userRoles.getUser().getId());
            query.setParameter("roleId", userRoles.getRole().getId());
            List<UserRoles> list = query.list();
            return list.get(0);
        } catch (Exception e){
            throw new EntityNotFoundException(NO_SUCH_USER_ROLE);
        }
    }
}
