package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.PassRestartRequest;
import com.example.smartgarage.models.User;

import java.util.List;

public interface RestartPasswordRepository {

    void createRestartPasswordRequest(PassRestartRequest passRestartRequest);

    PassRestartRequest getByUser(User user);

    void updateRequest(PassRestartRequest request);

    List<PassRestartRequest> getUncompletedRequests();

    List<PassRestartRequest> getAllPassRestartRequest();
}
