package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.ConfirmationToken;

public interface ConfirmationTokenRepository {

    void create(ConfirmationToken token);

    ConfirmationToken getToken(String token);

    public void setConfirmedAt(ConfirmationToken token);

}
