package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.filterParameters.ServiceOrderFilterParameters;
import com.example.smartgarage.repositories.contracts.ServiceOrderRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;
import static com.example.smartgarage.repositories.util.RepositoryHelper.parseOptionalStringToLocalDateTime;

@Repository
public class ServiceOrderRepositoryImpl implements ServiceOrderRepository {

    private final SessionFactory sessionFactory;

    public ServiceOrderRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ServiceOrder> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<ServiceOrder> query = session.createQuery(
                    "from ServiceOrder ", ServiceOrder.class);
            return query.list();
        }
    }

    @Override
    public ServiceOrder getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Query<ServiceOrder> query = session.createQuery(
                    "from ServiceOrder where id = :id", ServiceOrder.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException(String.format(NO_ORDER_WITH_ID, id));
            } else {
                return query.list().get(0);
            }
        }
    }

    @Override
    public void create(ServiceOrder serviceOrder) {
        try (Session session = sessionFactory.openSession()) {
            session.save(serviceOrder);
        }
    }

    @Override
    public void update(ServiceOrder serviceOrder) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.update(serviceOrder);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(ServiceOrder serviceOrder) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.delete(serviceOrder);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<ServiceOrder> getAllServicesForMonth(LocalDateTime currentDate,
                                                     LocalDateTime nextMonth) {
        try(Session session = sessionFactory.openSession()){
            Query<ServiceOrder> query = session.createQuery(
                            " select serviceorder from ServiceOrder as serviceorder " +
                            " join serviceorder.visit as visit " +
                            " where visit.startDate >= :currentDate " +
                            " and visit.startDate < :nextMonth ", ServiceOrder.class);
            query.setParameter("currentDate", currentDate);
            query.setParameter("nextMonth", nextMonth);
            return query.list();
        }
    }

    @Override
    public List<ServiceOrder> getAllServicesByVisitId(Integer visitId) {
        try(Session session = sessionFactory.openSession()){
            Query<ServiceOrder> query = session.createQuery(
                    " select serviceorder from ServiceOrder as serviceorder " +
                                " join serviceorder.visit as visit " +
                                " where visit.id = :visitId", ServiceOrder.class);
            query.setParameter("visitId", visitId);
            return query.list();
        }
    }

    @Override
    public List<ServiceOrder> getAllServicesByUserId(Integer userId) {
        try(Session session = sessionFactory.openSession()){
            Query<ServiceOrder> query = session.createQuery(
                            " select serviceorder from ServiceOrder as serviceorder " +
                            " join serviceorder.visit as visit" +
                            " join visit.vehicle as vehicle " +
                            " join vehicle.owner as user " +
                            " where user.id = :userId", ServiceOrder.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public List<ServiceOrder> filterByVehicleIdAndOrStartDate(ServiceOrderFilterParameters sofp) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = " select serviceorder from ServiceOrder as serviceorder " +
                    " join serviceorder.visit as visit " +
                    " join visit.vehicle as vehicle " +
                    " join vehicle.owner as user " +
                    " where ";

            List<String> filters = new ArrayList<>();

            sofp.getVehicleId().ifPresent(v -> filters.add(" vehicle.id = :vehicleIdSofp"));
            sofp.getStartDate().ifPresent(d -> filters.add(" visit.startDate > :startDateSofp"));

            if (!filters.isEmpty()){
                queryString = queryString + String.join(" and ", filters);
            }

            Query<ServiceOrder> query = session.createQuery(queryString, ServiceOrder.class);
            sofp.getVehicleId().ifPresent(id -> query.setParameter("vehicleIdSofp", id));
            if (sofp.getStartDate().isPresent()) {
                LocalDateTime startDateSOFP = parseOptionalStringToLocalDateTime(sofp.getStartDate());
                query.setParameter("startDateSofp", startDateSOFP);
            }


            return query.list();
        }
    }
}
