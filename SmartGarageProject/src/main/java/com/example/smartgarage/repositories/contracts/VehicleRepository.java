package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Vehicle;

import java.time.LocalDateTime;
import java.util.List;

public interface VehicleRepository extends CrudRepository<Vehicle>{

    Vehicle getByVin(String vin);

    List<Vehicle> getByOwnerId(Integer ownerId);

    List<Vehicle> getAllVehiclesByMonth(LocalDateTime currentMonth, LocalDateTime nextMonth);
}
