package com.example.smartgarage.repositories.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class RepositoryHelper {

    public static LocalDateTime parseOptionalStringToLocalDateTime(Optional<String> date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateString = date.get();
        LocalDateTime localDateTime = LocalDateTime.parse(dateString, formatter);
        return localDateTime;
    }

    public static LocalDateTime parseOptionalStringToLocalDateTime(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateString = date;
        LocalDateTime localDateTime = LocalDateTime.parse(dateString, formatter);
        return localDateTime;
    }

    public static LocalDateTime concatenateYearAndMonthFromFrontEnd(String year, String month) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String fullDate = year + "-" + month + "-" + "01" + " " + "00:00:00";
        LocalDateTime localDateTime = LocalDateTime.parse(fullDate, formatter);
        return localDateTime;
    }
}
