package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.ConfirmationToken;
import com.example.smartgarage.repositories.contracts.ConfirmationTokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ConfirmationTokenRepositoryImpl implements ConfirmationTokenRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public ConfirmationTokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(ConfirmationToken token) {
        try (Session session = sessionFactory.openSession()) {
            session.save(token);
        }
    }

    @Override
    public ConfirmationToken getToken(String tokenToGet){
        try (Session session = sessionFactory.openSession()) {
            Query<ConfirmationToken> query = session.createQuery(
            "from ConfirmationToken where token like :tokenToGet", ConfirmationToken.class);
            query.setParameter("tokenToGet", tokenToGet);

            List<ConfirmationToken> tokenOut = new ArrayList<>(query.list());
            return tokenOut.get(0);
        } catch (Exception e){
            throw new EntityNotFoundException("Token not found.");
        }
    }

    @Override
    public void setConfirmedAt(ConfirmationToken token) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(token);
            session.getTransaction().commit();
        }
    }
}
