package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Visit;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface VisitRepository extends CrudRepository<Visit>{

    List<Visit> getVisitByStartAndEndDate(Optional<String> startDate, Optional<String> endDate);

    List<Visit> getAllVisitsByMonth(LocalDateTime currentMonth, LocalDateTime nextMonth);

    List<Visit> getAllVisitsByVehicleId(String vehicleId);

    List<Visit> getAllActiveVisits();

}
