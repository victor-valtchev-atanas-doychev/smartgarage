package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.ShopService;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ShopServiceFilterParameters;

import java.util.List;
import java.util.Optional;

public interface ShopServiceRepository extends CrudRepository<ShopService>{
    List<ShopService> getShopServicesByNameAndPrice(ShopServiceFilterParameters ssfs);

    List<ShopService> getAllServicesLinkedToACustomer(User user, ShopServiceFilterParameters ssfs);
}
