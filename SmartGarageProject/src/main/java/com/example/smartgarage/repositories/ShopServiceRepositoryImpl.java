package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.ShopService;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.filterParameters.ShopServiceFilterParameters;
import com.example.smartgarage.repositories.contracts.ShopServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;
import static com.example.smartgarage.repositories.util.RepositoryHelper.parseOptionalStringToLocalDateTime;

@Repository
public class ShopServiceRepositoryImpl implements ShopServiceRepository {

    private final SessionFactory sessionFactory;

    public ShopServiceRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ShopService> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<ShopService> query = session.createQuery(
                    "from ShopService ", ShopService.class);
            return query.list();
        }
    }

    @Override
    public ShopService getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<ShopService> query = session.createQuery(
                    "from ShopService where id = :id", ShopService.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format(NO_SERVICE_WITH_ID, id));
            } else {
                return query.list().get(0);
            }
        }
    }

    @Override
    public void create(ShopService shopService) {
        try (Session session = sessionFactory.openSession()) {
            session.save(shopService);
        }
    }

    @Override
    public void update(ShopService shopService) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.update(shopService);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(ShopService shopService) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.delete(shopService);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<ShopService> getShopServicesByNameAndPrice(ShopServiceFilterParameters sfp) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from ShopService where";

            List<String> filters = new ArrayList<>();

            sfp.getName().ifPresent(n -> filters.add(" name = :nameSFP"));
            sfp.getPrice().ifPresent(p -> filters.add(" price = :priceSFP"));

            if (!filters.isEmpty()){
                queryString = queryString + String.join(" and ", filters);
            }

            Query<ShopService> query = session.createQuery(queryString, ShopService.class);
            sfp.getName().ifPresent(name -> query.setParameter("nameSFP", name));
            sfp.getPrice().ifPresent(price -> query.setParameter("priceSFP", price));

            return query.list();
        }
    }

    @Override
    public List<ShopService> getAllServicesLinkedToACustomer(User user, ShopServiceFilterParameters sfp) {
        try (Session session = sessionFactory.openSession()) {
            int userId = user.getId();
            String queryString =
                    "select shopservice from ServiceOrder as serviceorder " +
                    " join serviceorder.service as shopservice " +
                    " join serviceorder.visit as visit " +
                    " where visit.vehicle.owner.id = :userId";

            List<String> filters = new ArrayList<>();

            sfp.getVehicleId().ifPresent(v -> filters.add(" visit.vehicle.id = :vehicleSFP "));
            sfp.getStartDate().ifPresent(s -> filters.add(" visit.startDate < :startDateSFP "));
            sfp.getEndDate().ifPresent(e -> filters.add(" visit.endDate > :endDateSFP "));

            if (!filters.isEmpty()){
                queryString = queryString + " and " + String.join(" and ", filters);
            }

            Query<ShopService> query = session.createQuery(queryString, ShopService.class);

            query.setParameter("userId", userId);
            sfp.getVehicleId().ifPresent(vehicle -> query.setParameter("vehicleSFP", vehicle));

            if (sfp.getStartDate().isPresent()){
                LocalDateTime startDateSFP = parseOptionalStringToLocalDateTime(sfp.getStartDate());
                query.setParameter("startDateSFP", startDateSFP);
            }

            if (sfp.getEndDate().isPresent()){
                LocalDateTime endDateSFP = parseOptionalStringToLocalDateTime(sfp.getEndDate());
                query.setParameter("endDateSFP", endDateSFP);
            }

            return query.list();
        }
    }
}
