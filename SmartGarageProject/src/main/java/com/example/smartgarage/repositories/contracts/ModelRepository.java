package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Model;

public interface ModelRepository extends CrudRepository<Model>{

}
