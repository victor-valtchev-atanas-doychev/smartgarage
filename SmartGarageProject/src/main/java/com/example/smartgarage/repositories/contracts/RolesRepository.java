package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Role;

public interface RolesRepository {

    Role getById(int id);
}
