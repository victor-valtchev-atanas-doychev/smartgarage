package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.exceptions.OrderOfOperationsException;
import com.example.smartgarage.exceptions.WrongParametersException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.filterParameters.CustomersSortParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;
import com.example.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.*;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;
import static com.example.smartgarage.repositories.util.RepositoryHelper.parseOptionalStringToLocalDateTime;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_REGISTERED_USERS);
            }
            return query.list();
        }
    }

    @Override
    public List<User> getAllCustomersWithSort(CustomersSortParameters csp) {
        try (Session session = sessionFactory.openSession()) {

            String queryString =
                    "select distinct user from Visit visit " +
                            " join visit.vehicle as vehicle " +
                            " join vehicle.owner as user " +
                            " join user.roles as role " +
                            " where role.role = 'Customer'";

            List<String> filters = new ArrayList<>();

            csp.getNameOrder().ifPresent(n -> filters.add(
                    " order by user.firstName " + csp.getNameOrder().get()));
            csp.getDateOrder().ifPresent(d -> filters.add(
                    " order by visit.startDate " + csp.getDateOrder().get()));

            if (filters.size() == 2) {
                throw new WrongParametersException(CANNOT_FILTER_BY_TWO_PARAMETERS);
            } else if (filters.size() == 1) {
                queryString = queryString + filters.get(0);
            }

            Query<User> query = session.createQuery(queryString, User.class);

            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where id = :id", User.class);
            query.setParameter("id", id);
            List<User> list = query.list();
            return list.get(0);
        } catch (Exception e) {
            throw new EntityNotFoundException(String.format(NO_USER_WITH_ID, id));
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> list = query.list();
            return list.get(0);
        } catch (Exception e) {
            throw new EntityNotFoundException(
                    String.format(USER_WITH_SUCH_EMAIL_NOT_EXIST, email));
        }
    }

    @Override
    public User getUserByPhone(String phone) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where phone = :phone", User.class);
            query.setParameter("phone", phone);
            List<User> list = query.list();
            return list.get(0);
        } catch (Exception e) {
            throw new EntityNotFoundException(
                    String.format(USER_WITH_PHONE_NOT_EXIST, phone));
        }
    }

    @Override
    public User getUserByPassword(String password) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where password = :password", User.class);
            query.setParameter("password", password);
            List<User> list = query.list();
            return list.get(0);
        } catch (Exception e) {
            throw new EntityNotFoundException(WRONG_PASSWORD);
        }
    }

    @Override
    public List<User> filter(UserFilterParameters ufp) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "";

            String queryStringWithDates =
                            "select distinct user from Visit visit " +
                            " join visit.vehicle as vehicle " +
                            " join vehicle.owner as user " +
                            " join user.roles as role " +
                            " where role.role = 'Customer'";

            String queryStringWithModelMake =
                            " select distinct user from Vehicle as vehicle" +
                            " join vehicle.owner as user " +
                            " join user.roles as role " +
                            " where role.role = 'Customer' ";

            String queryStandard =
                            " select distinct user from User as user " +
                            " join user.roles as role " +
                            " where role.role = 'Customer' ";

            if (ufp.getVisitStart().isPresent() || ufp.getVisitEnd().isPresent()) {
                queryString = queryStringWithDates;
            } else if (ufp.getModel().isPresent() || ufp.getMake().isPresent()) {
                queryString = queryStringWithModelMake;
            } else {
                queryString = queryStandard;
            }

            List<String> filters = new ArrayList<>();

            ufp.getFirstName().ifPresent(f -> filters.add(" user.firstName like :firstNameUFP "));
            ufp.getLastName().ifPresent(l -> filters.add(" user.lastName like :lastNameUFP "));
            ufp.getEmail().ifPresent(e -> filters.add(" user.email like :emailUFP "));
            ufp.getPhone().ifPresent(p -> filters.add(" user.phone like :phoneUFP "));
            ufp.getModel().ifPresent(m -> filters.add(" vehicle.model.modelName like :modelNameUFP "));
            ufp.getMake().ifPresent(b -> filters.add(" vehicle.model.brand.brandName like :brandNameUFP "));
            ufp.getVisitStart().ifPresent(st -> filters.add(" visit.startDate > :startDateUFP "));
            ufp.getVisitEnd().ifPresent(en -> filters.add(" visit.endDate < :endDateUFP "));

            if (!filters.isEmpty()) {
                queryString = queryString + " and " + String.join(" and ", filters);
            } else {
                throw new WrongParametersException(NO_FILTER_PARAMS);
            }

            Query<User> query = session.createQuery(queryString, User.class);

            ufp.getFirstName().ifPresent(firstName -> query.setParameter("firstNameUFP", "%"+firstName+"%"));
            ufp.getLastName().ifPresent(lastName -> query.setParameter("lastNameUFP", "%"+lastName+"%"));
            ufp.getEmail().ifPresent(email -> query.setParameter("emailUFP", "%"+email+"%"));
            ufp.getPhone().ifPresent(phone -> query.setParameter("phoneUFP", "%"+phone+"%"));
            ufp.getModel().ifPresent(modelName -> query.setParameter("modelNameUFP", "%"+modelName+"%"));
            ufp.getMake().ifPresent(brandName -> query.setParameter("brandNameUFP", "%"+brandName+"%"));

            if (ufp.getVisitStart().isPresent()) {
                LocalDateTime startDateUFP = parseOptionalStringToLocalDateTime(ufp.getVisitStart());
                query.setParameter("startDateUFP", startDateUFP);
            }

            if (ufp.getVisitEnd().isPresent()) {
                LocalDateTime endDateUFP = parseOptionalStringToLocalDateTime(ufp.getVisitEnd());
                query.setParameter("endDateUFP", endDateUFP);
            }
            return query.list();
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery(
                    "from Role where id = 1", Role.class);
            UserRoles userRoles = new UserRoles();
            session.save(user);
            userRoles.setUser(user);
            userRoles.setRole(query.list().get(0));
            session.save(userRoles);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(User user) {
        try (Session session = sessionFactory.openSession()) {
            int userId = user.getId();
            User userToDelete = getById(userId);
            Query<Vehicle> query = session.createQuery(
                    "from Vehicle where owner.id = :id");
            query.setParameter("id", userId);

            if (!query.list().isEmpty()) {
                throw new OrderOfOperationsException
                        (USER_CANNOT_BE_DELETED_IF_VEHICLES_LINKED);
            } else
                session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public void checkIfEmailExists(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where email = :email", User.class);
            query.setParameter("email", email);
        } catch (Exception e) {
            throw new EntityNotFoundException(EMAIL_DOES_NOT_EXIST);
        }
    }

    @Override
    public void checkIfMailAndPhoneMatch(String email, String phone) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where email = :email and phone = :phone", User.class);
            query.setParameter("email", email);
            query.setParameter("phone", phone);
        } catch (Exception e) {
            throw new EntityNotFoundException(
                    NO_USER_WITH_EMAIL_AND_PHONE, email, phone);
        }
    }

    @Override
    public User getUserById(int emailId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where id = :emailId", User.class);
            query.setParameter("emailId", emailId);
            List<User> list = query.list();
            return list.get(0);
        } catch (Exception e) {
            throw new EntityNotFoundException(String.format(USER_WITH_ID_NOT_EXIST, emailId));
        }
    }
}

