package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Model;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;

@Repository
public class ModelRepositoryImpl implements ModelRepository {

    private final SessionFactory sessionFactory;

    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Model> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery(
                    "from Model", Model.class);
            return query.list();
        } catch (Exception e) {
            throw new EntityNotFoundException(MODELS_NOT_EXIST);
        }
    }

    @Override
    public Model getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery(
                    "from Model where id = :id", Model.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(
                        String.format(MODEL_WITH_ID_NOT_EXIST, id));
            }
            return query.list().get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void create(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.save(model);
        }
    }

    @Override
    public void update(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.update(model);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Model model) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.delete(model);
            session.getTransaction().commit();
        }
    }
}
