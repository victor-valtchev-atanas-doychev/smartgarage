package com.example.smartgarage.repositories.util;

public class RepositoryConstants {

    public static final String NO_BRANDS_ADD_BRANDS =
            "There are no brand created yet. Please add brands first.";

    public static final String BRAND_DOES_NOT_EXIST =
            "Brand with id %d does not exist.";

    public static final String MODELS_NOT_EXIST =
            "No models exist in the database yet.";

    public static final String MODEL_WITH_ID_NOT_EXIST =
            "Model with ID %d does not exist.";

    public static final String PASS_REQUEST_NOT_EXIST =
            "This restart password request does not exists.";

    public static final String NO_UNCOMPLETED_PASS_REQUESTS =
            "There are no uncompleted pass restart requests.";

    public static final String NO_PASS_RESTART_REQUESTS =
            "There are no records of pass restart requests.";

    public static final String ROLE_WITH_ID_NOT_EXISTS =
            "Role with such ID does not exist.";

    public static final String NO_ORDER_WITH_ID =
            "There is no Service Order with id %d";

    public static final String NO_SERVICE_WITH_ID =
            "There is no Service with id %d.";

    public static final String NO_REGISTERED_USERS =
            "There are no users registered.";

    public static final String CANNOT_FILTER_BY_TWO_PARAMETERS =
            "Cannot filter by two parameters.";

    public static final String NO_USER_WITH_ID =
            "User with such ID:%s does not exist.";

    public static final String USER_WITH_SUCH_EMAIL_NOT_EXIST =
            "User with such email:%s does not exist.";

    public static final String USER_WITH_PHONE_NOT_EXIST =
            "User with such phone:%s does not exist.";

    public static final String WRONG_PASSWORD =
            "Wrong password";

    public static final String USER_CANNOT_BE_DELETED_IF_VEHICLES_LINKED =
            "User cannot be deleted, if there are vehicles linked to it.";

    public static final String EMAIL_DOES_NOT_EXIST =
            "This email does not exist.";

    public static final String NO_USER_WITH_EMAIL_AND_PHONE =
            "There is no user with these email:%s and phone:%s.";

    public static final String USER_WITH_ID_NOT_EXIST =
            "User with such id:%s does not exist.";

    public static final String NO_FILTER_PARAMS =
            "There is no filter parameters.";

    public static final String NO_USER_ROLES =
            "There are no user-roles registered.";

    public static final String NO_SUCH_USER_ROLE =
            "Such user-role relation does not exist.";

    public static final String NO_VEHICLES =
            "There are no Vehicles to show.";

    public static final String NO_VEHICLE_WITH_ID =
            "Vehicle with id %d not found.";

    public static final String NO_VEHICLE_WITH_VIN =
            "Vehicle with vin:%s not found.";

    public static final String CLIENT_HAVE_NO_VEHICLES =
            "Client with id %d does not have any vehicles.";

    public static final String NO_VISITS =
            "There are no registered visits yet.";

    public static final String NO_VISITS_WITH_ID =
            "There is no visit with id %d.";
}
