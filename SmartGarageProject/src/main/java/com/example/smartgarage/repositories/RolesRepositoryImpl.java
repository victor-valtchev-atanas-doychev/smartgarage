package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Role;
import com.example.smartgarage.repositories.contracts.RolesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;

@Repository
public class RolesRepositoryImpl implements RolesRepository {

    private final SessionFactory sessionFactory;

    public RolesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery(
                    "from Role where id = :id", Role.class);
            query.setParameter("id", id);
            List<Role> list = query.list();
            return list.get(0);
        } catch (Exception e) {
            throw new EntityNotFoundException(ROLE_WITH_ID_NOT_EXISTS);
        }
    }
}
