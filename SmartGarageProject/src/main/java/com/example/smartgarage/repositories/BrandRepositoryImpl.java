package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Brand;
import com.example.smartgarage.repositories.contracts.BrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;

@Repository
public class BrandRepositoryImpl implements BrandRepository {

    private final SessionFactory sessionFactory;

    public BrandRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brand> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<Brand> query = session.createQuery("from Brand", Brand.class);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException(NO_BRANDS_ADD_BRANDS);
            }
            return query.list();
        }
    }

    @Override
    public Brand getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Query<Brand> query = session.createQuery("from Brand where id = :id", Brand.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException(String.format(BRAND_DOES_NOT_EXIST, id));
            }
            return query.list().get(0);
        }
    }

    @Override
    public Brand getByName(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Brand> query = session.createQuery(
                    "from Brand where brandName = :name", Brand.class);
            query.setParameter("name", name);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException(String.format(
                        "Brand with name %s does not exist.", name));
            }
            return query.list().get(0);
        }
    }

    @Override
    public void create(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brand);
        }
    }

    @Override
    public void update(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.update(brand);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.delete(brand);
            session.getTransaction().commit();
        }
    }
}
