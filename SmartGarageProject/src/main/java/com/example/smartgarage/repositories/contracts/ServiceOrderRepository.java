package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.ServiceOrder;
import com.example.smartgarage.models.filterParameters.ServiceOrderFilterParameters;

import java.time.LocalDateTime;
import java.util.List;

public interface ServiceOrderRepository extends CrudRepository<ServiceOrder>{
    List<ServiceOrder> getAllServicesForMonth(LocalDateTime date, LocalDateTime nextMonth);

    List<ServiceOrder> getAllServicesByVisitId(Integer visitId);

    List<ServiceOrder> getAllServicesByUserId(Integer userId);

    List<ServiceOrder> filterByVehicleIdAndOrStartDate(ServiceOrderFilterParameters sofp);
}
