package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.PassRestartRequest;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserInfoUpdateDto;
import com.example.smartgarage.models.filterParameters.CustomersSortParameters;
import com.example.smartgarage.models.filterParameters.UserFilterParameters;

import java.util.List;

public interface UserRepository extends CrudRepository<User>{

    List<User> getAllCustomersWithSort(CustomersSortParameters csp);

    User getUserByEmail(String email);

    User getUserByPhone(String phone);

    List<User> filter(UserFilterParameters ufp);

    void checkIfEmailExists(String email);

    void checkIfMailAndPhoneMatch(String email, String phone);

    User getUserById(int emailId);

    User getUserByPassword(String password);
}
