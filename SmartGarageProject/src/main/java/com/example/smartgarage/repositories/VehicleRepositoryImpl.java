package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Vehicle;
import com.example.smartgarage.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;

@Repository
public class VehicleRepositoryImpl implements VehicleRepository {

    private final SessionFactory sessionFactory;

    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    // To be added a limit on how much data is being sent. Ex: timeframe.
    @Override
    public List<Vehicle> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery("from Vehicle", Vehicle.class);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_VEHICLES);
            }
            return query.list();
        }
    }

    @Override
    public Vehicle getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery(
                    "from Vehicle where id = :id", Vehicle.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format(
                        NO_VEHICLE_WITH_ID, id));
            }
            return query.list().get(0);
        }
    }

    @Override
    public Vehicle getByVin(String vin) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery(
                    "from Vehicle where vin = :vin", Vehicle.class);
            query.setParameter("vin", vin);

            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format(
                        NO_VEHICLE_WITH_VIN, vin));
            }
            return query.list().get(0);
        }
    }

    @Override
    public List<Vehicle> getByOwnerId(Integer ownerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery(
                    "from Vehicle where owner.id = :ownerId", Vehicle.class);
            query.setParameter("ownerId", ownerId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format(
                        CLIENT_HAVE_NO_VEHICLES, ownerId));
            }
            return query.list();
        }
    }

    @Override
    public List<Vehicle> getAllVehiclesByMonth(LocalDateTime currentMonth, LocalDateTime nextMonth) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicle> query = session.createQuery(
                            " select distinct vehicle from Visit as visit " +
                            " join visit.vehicle as vehicle " +
                            " where visit.startDate >= :currentMonth " +
                            " and visit.startDate < :nextMonth ", Vehicle.class);
            query.setParameter("currentMonth", currentMonth);
            query.setParameter("nextMonth", nextMonth);
            return query.list();
        }
    }

    @Override
    public void create(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.save(vehicle);
        }
    }

    @Override
    public void update(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.update(vehicle);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            session.clear();
            session.beginTransaction();
            session.delete(vehicle);
            session.getTransaction().commit();
        }
    }
}
