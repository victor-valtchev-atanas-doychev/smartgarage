package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.exceptions.WrongParametersException;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.smartgarage.repositories.util.RepositoryConstants.*;
import static com.example.smartgarage.repositories.util.RepositoryHelper.parseOptionalStringToLocalDateTime;

@Repository
public class VisitRepositoryImpl implements VisitRepository {

    private final SessionFactory sessionFactory;

    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    // To be added a limit on how much data is being sent. Ex: timeframe.
    @Override
    public List<Visit> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<Visit> query = session.createQuery("from Visit", Visit.class);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException(NO_VISITS);
            } else {
                return query.list();
            }
        }
    }

    @Override
    public Visit getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Query<Visit> query = session.createQuery(
                    "from Visit where id = :id", Visit.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException(
                        String.format(NO_VISITS_WITH_ID, id));
            } else {
                return query.list().get(0);
            }
        }
    }

    @Override
    public void create(Visit visit) {
        try (Session session = sessionFactory.openSession()){
            session.save(visit);
        }
    }

    @Override
    public void update(Visit visit) {
        try(Session session = sessionFactory.openSession()){
            session.clear();
            session.beginTransaction();
            session.update(visit);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Visit visit) {
        try(Session session = sessionFactory.openSession()){
            session.clear();
            session.beginTransaction();
            session.delete(visit);
            session.getTransaction().commit();
        }
    }


    @Override
    public List<Visit> getVisitByStartAndEndDate(Optional<String> fromDate,
                                                 Optional<String> toDate) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = " from Visit where ";

            List<String> filters = new ArrayList<>();

            fromDate.ifPresent(s -> filters.add(" startDate >= :fromDate "));
            toDate.ifPresent(e -> filters.add(" endDate < :toDate"));

            if (!filters.isEmpty()) {
                queryString = queryString + String.join(" and ", filters);
            } else {
                throw new WrongParametersException("There is no filter parameters.");
            }

            Query<Visit> query = session.createQuery(queryString, Visit.class);
            if (fromDate.isPresent()){
                LocalDateTime parsedFromDate = parseOptionalStringToLocalDateTime(fromDate);
                query.setParameter("fromDate", parsedFromDate);
            }
            if (toDate.isPresent()){
                LocalDateTime parsedToDate = parseOptionalStringToLocalDateTime(toDate);
                query.setParameter("toDate", parsedToDate);
            }

            return query.list();
        }
    }

    @Override
    public List<Visit> getAllVisitsByMonth(LocalDateTime currentMonth,
                                           LocalDateTime nextMonth){
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(
                            "from Visit" +
                            " where startDate >= :currentMonth " +
                            " and startDate < :nextMonth ", Visit.class);
            query.setParameter("currentMonth", currentMonth);
            query.setParameter("nextMonth", nextMonth);
            return query.list();
        }
    }

    @Override
    public List<Visit> getAllVisitsByVehicleId(String vehicleId) {
        Integer integer = Integer.parseInt(vehicleId);
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(
                            " select visit from Visit as visit " +
                            " join visit.vehicle as vehicle " +
                            " where vehicle.id = :integer" +
                            " order by visit.startDate desc ", Visit.class);
            query.setParameter("integer", integer);
            return query.list();
        }
    }

    @Override
    public List<Visit> getAllActiveVisits() {
        try(Session session = sessionFactory.openSession()){
            Query<Visit> query = session.createQuery(
                    "from Visit where endDate = null", Visit.class);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException(NO_VISITS);
            } else {
                return query.list();
            }
        }
    }
}
