package com.example.smartgarage.repositories.contracts;

import java.util.List;

public interface CrudRepository<T> {

    List<T> getAll();

    T getById(int id);

    void create(T object);

    void update(T object);

    void delete(T object);
}
