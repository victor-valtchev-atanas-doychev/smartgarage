package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Brand;

public interface BrandRepository extends CrudRepository<Brand>{

    Brand getByName(String name);
}
