package com.example.smartgarage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = AuthenticationFailureException.class)
    protected ResponseEntity<Object> handleApiRequestException(AuthenticationFailureException e){

        HttpStatus conflict = HttpStatus.CONFLICT;
        ApiException apiException = new ApiException(e.getMessage(), e, conflict);
        return new ResponseEntity<>(apiException, conflict);
    }

    @ExceptionHandler(value = DuplicateEntityException.class)
    protected ResponseEntity<Object> handleApiRequestException(DuplicateEntityException e){

        HttpStatus conflict = HttpStatus.CONFLICT;
        ApiException apiException = new ApiException(e.getMessage(), e, conflict);
        return new ResponseEntity<>(apiException, conflict);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    protected ResponseEntity<Object> handleApiRequestException(EntityNotFoundException e){

        //must be NOT_FOUND to show the message. With NO_CONTENT it shows "1" instead the message
        HttpStatus notFound = HttpStatus.NOT_FOUND;
        ApiException apiException = new ApiException(e.getMessage(), e, notFound);
        return new ResponseEntity<>(apiException, notFound);
    }

    @ExceptionHandler(value = OrderOfOperationsException.class)
    protected ResponseEntity<Object> handleApiRequestException(OrderOfOperationsException e){

        HttpStatus conflict = HttpStatus.CONFLICT;
        ApiException apiException = new ApiException(e.getMessage(), e, conflict);
        return new ResponseEntity<>(apiException, conflict);
    }

    @ExceptionHandler(value = UnauthorizedOperationException.class)
    protected ResponseEntity<Object> handleApiRequestException(UnauthorizedOperationException e){

        HttpStatus unauthorized = HttpStatus.UNAUTHORIZED;
        ApiException apiException = new ApiException(e.getMessage(), e, unauthorized);
        return new ResponseEntity<>(apiException, unauthorized);
    }

    @ExceptionHandler(value = ResponseStatusException.class)
    protected ResponseEntity<Object> handleApiRequestException(ResponseStatusException e){

        HttpStatus unauthorized = HttpStatus.UNAUTHORIZED;
        ApiException apiException = new ApiException(e.getMessage(), e, unauthorized);
        return new ResponseEntity<>(apiException, unauthorized);
    }

    @ExceptionHandler(value = WrongCarRegistrationPlateException.class)
    protected ResponseEntity<Object> handleApiRequestException(WrongCarRegistrationPlateException e){

        HttpStatus conflict = HttpStatus.CONFLICT;
        ApiException apiException = new ApiException(e.getMessage(), e, conflict);
        return new ResponseEntity<>(apiException, conflict);
    }

    @ExceptionHandler(value = WrongParametersException.class)
    protected ResponseEntity<Object> handleApiRequestException(WrongParametersException e){

        HttpStatus conflict = HttpStatus.CONFLICT;
        ApiException apiException = new ApiException(e.getMessage(), e, conflict);
        return new ResponseEntity<>(apiException, conflict);
    }
}
