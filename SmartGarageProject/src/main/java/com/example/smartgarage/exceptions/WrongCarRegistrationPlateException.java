package com.example.smartgarage.exceptions;

public class WrongCarRegistrationPlateException extends RuntimeException{
    public WrongCarRegistrationPlateException(String message) {
        super(message);
    }
}
