package com.example.smartgarage.exceptions;

public class AuthenticationFailureException extends RuntimeException{
    public AuthenticationFailureException() {
    }

    public AuthenticationFailureException(String message) {
        super(message);
    }
}
