package com.example.smartgarage.exceptions;

public class OrderOfOperationsException extends RuntimeException{
    public OrderOfOperationsException(String message) {
        super(message);
    }
}
