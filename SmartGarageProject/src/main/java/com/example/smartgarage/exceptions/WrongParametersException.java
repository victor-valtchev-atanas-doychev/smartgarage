package com.example.smartgarage.exceptions;

public class WrongParametersException extends RuntimeException{
    public WrongParametersException(String message) {
        super(message);
    }
}
