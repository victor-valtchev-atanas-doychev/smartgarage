package com.example.smartgarage.exceptions;

public class DuplicateEntityException extends RuntimeException{

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
    }

    public DuplicateEntityException(String type, String attribute) {
        super(String.format("%s with %s already exists.", type, attribute));
    }

    public DuplicateEntityException(String emailOrPhone) {
        super(String.format("User with email or phone [ %s ] already exists.", emailOrPhone));
    }
}
