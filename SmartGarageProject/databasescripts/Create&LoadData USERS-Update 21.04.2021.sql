-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.5.8-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дъмп на структурата на БД smartgarage
CREATE DATABASE IF NOT EXISTS `smartgarage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smartgarage`;

-- Дъмп структура за таблица smartgarage.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_uindex` (`email`),
  UNIQUE KEY `user_phone_uindex` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.users: ~25 rows (приблизително)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `first_name`, `last_name`, `phone`, `email`, `password`, `enabled`) VALUES
	(1, 'Victor', 'Valtchev', '0886634419', 'v.v@gmail.com', '123456789', 1),
	(2, 'Atanas', 'Doychev', '0882345127', 'a.d@gmail.com', '123456789', 1),
	(3, 'Stefan', 'Petrov', '0887623442', 'admin@gmail.com', '123456789', 1),
	(4, 'Ralica', 'Ivanova', '0899999999', 'boss@gmail.com', '123456789', 1),
	(5, 'Stoqn', 'Dimitrov', '0892345123', 'customer.employee@gmail.com', '1234567890', 1),
	(6, 'Rosen', 'Petrov', '0884444448', 'rosen.p@gmail.com', '123456789', 1),
	(13, 'Silviq', 'Raleva', '0884444445', 's.raleva@gmail.com', '123456789', 1),
	(16, 'Qvor', 'Kolev', '0884444666', 'qvor.kolev@gmail.com', '123456789', 1),
	(17, 'Petq', 'Petrova', '0884444777', 'petq.p.p@gmail.com', '123456789', 1),
	(18, 'Victor', 'Vitkov', '0884444444', 'vic.vit@gmail.com', '123456789', 1),
	(19, 'Ivan', 'Dimitrov', '0884444789', 'dimitrov@gmail.com', '123456789', 1),
	(20, 'Locan', 'Petrov', '0884444555', 'lozan@gmail.com', '123456789', 1),
	(21, 'Chudomir', 'Manov', '0884441000', 'chudomir@abv.bg', '123456789', 0),
	(22, 'Katq', 'Kirilova', '0884442222', 'kateto@abv.bg', '123456789', 0),
	(23, 'Roman', 'Georgiev', '0884443333', 'roman.g@abv.bg', '123456789', 0),
	(24, 'Hristo', 'Stoqnov', '0884441234', 'hristo.s@abv.bg', '123456789', 0),
	(25, 'Veronika', 'Milanova', '0884449999', 'v.milanova@abv.bg', '123456789', 0),
	(26, 'Atanas', 'Atanasov', '0884440000', 'a.atanasov@abv.bg', '123456789', 0),
	(27, 'Rumen', 'Radev', '0884444567', 'r.radev@abv.bg', '123456789', 1),
	(28, 'Momchil', 'Genov', '0882220987', 'momchil.g@abv.bg', '123456789', 1),
	(29, 'Liliq', 'Koleva', '0880000023', 'liliq.koleva@gmail.com', '123456789', 1),
	(30, 'Rosen', 'Vanchev', '0000021333', 'Rosen.van@gmail.com', '123456789', 0),
	(31, 'Rangel', 'Todorov', '0665432444', 'rangel.t@gmail.com', '123456789', 0),
	(32, 'Ivelin', 'Rachkov', '0885246230', 'rachkov.i@gmail.com', 'c3e3eca7-7868-40ad-9e4f-3c4af54bfa95', 0),
	(33, 'Stoqnka', 'Petrova', '0554327888', 'smart.garage.first.customer@gmail.com', 'secondpasschange', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
