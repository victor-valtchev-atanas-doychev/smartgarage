-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.5.8-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дъмп на структурата на БД smartgarage
DROP DATABASE IF EXISTS `smartgarage`;
CREATE DATABASE IF NOT EXISTS `smartgarage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smartgarage`;

-- Дъмп структура за таблица smartgarage.car_brands
DROP TABLE IF EXISTS `car_brands`;
CREATE TABLE IF NOT EXISTS `car_brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.car_brands: ~4 rows (приблизително)
/*!40000 ALTER TABLE `car_brands` DISABLE KEYS */;
INSERT INTO `car_brands` (`id`, `name`) VALUES
	(1, 'Mercedes-Benz'),
	(4, 'Range Rover'),
	(2, 'Rimac'),
	(3, 'Toyota');
/*!40000 ALTER TABLE `car_brands` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.car_models
DROP TABLE IF EXISTS `car_models`;
CREATE TABLE IF NOT EXISTS `car_models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_models_car_brands_id_fk` (`brand_id`),
  CONSTRAINT `car_models_car_brands_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `car_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.car_models: ~5 rows (приблизително)
/*!40000 ALTER TABLE `car_models` DISABLE KEYS */;
INSERT INTO `car_models` (`id`, `brand_id`, `name`) VALUES
	(1, 1, 'S-class 600'),
	(3, 1, 'E-class 320'),
	(4, 2, 'Concept Two'),
	(5, 3, 'Yaris'),
	(6, 2, 'Concept one');
/*!40000 ALTER TABLE `car_models` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.confirmation_tokens
DROP TABLE IF EXISTS `confirmation_tokens`;
CREATE TABLE IF NOT EXISTS `confirmation_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `expires_at` datetime NOT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `confirmation_tokens_users_id_fk` (`user_id`),
  CONSTRAINT `confirmation_tokens_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.confirmation_tokens: ~21 rows (приблизително)
/*!40000 ALTER TABLE `confirmation_tokens` DISABLE KEYS */;
INSERT INTO `confirmation_tokens` (`id`, `token`, `created_at`, `expires_at`, `confirmed_at`, `user_id`) VALUES
	(1, '030e535f-b6ae-4b25-a349-80664a6d0140', '2021-04-11 19:52:29', '2021-04-12 19:52:29', NULL, 20),
	(2, '7da39956-f1f6-480d-bc4f-d78f153a54d2', '2021-04-14 12:14:40', '2021-04-15 12:14:40', NULL, 22),
	(3, 'ef11630a-42d2-4e29-b0aa-779f0f51a011', '2021-04-14 12:17:50', '2021-04-15 12:17:50', NULL, 23),
	(4, 'e128d505-4119-4b19-a120-df39a7dba67e', '2021-04-14 12:20:01', '2021-04-15 12:20:01', NULL, 24),
	(5, '997ea86e-fed1-4c0c-bc7a-19146306a391', '2021-04-14 12:25:25', '2021-04-15 12:25:25', NULL, 25),
	(6, '531d6f4a-5857-460d-9c90-c14ceda8a3b0', '2021-04-14 12:38:17', '2021-04-15 12:38:17', NULL, 26),
	(7, '023d3dd9-f309-41b4-b955-f33c4526bb9f', '2021-04-14 13:11:32', '2021-04-15 13:11:32', '2021-04-14 13:12:15', 27),
	(8, '80aba2f5-fd72-4e94-916d-b359e49ac865', '2021-04-14 13:14:00', '2021-04-15 13:14:00', '2021-04-14 13:14:24', 28),
	(9, '29da1ac6-6379-4bb6-9f26-dcf54bcd95ad', '2021-04-14 13:42:12', '2021-04-15 13:42:12', '2021-04-14 13:45:33', 29),
	(10, 'f8c1636d-4171-4474-a57a-c3e5fb63ff06', '2021-04-15 17:18:02', '2021-04-15 23:59:02', '2021-04-16 16:48:56', 29),
	(11, 'f8c23c49-7930-4f77-b2df-72e34ff3fd20', '2021-04-16 12:38:25', '2021-04-17 12:38:25', NULL, 31),
	(12, '56f9816d-ed25-4cf7-9b4c-1606337ebe86', '2021-04-16 18:26:32', '2021-04-16 19:26:32', '2021-04-16 18:28:17', 31),
	(13, '833f0a79-fca7-409d-b079-a978e93cc132', '2021-04-17 18:29:27', '2021-04-17 19:29:27', NULL, 31),
	(14, '47bd0f95-565c-4bc5-ab9c-c200da756260', '2021-04-18 18:26:22', '2021-04-18 19:26:22', NULL, 31),
	(15, '425fcdc7-6e78-44e2-879f-afa45a0f7a88', '2021-04-18 18:28:56', '2021-04-18 19:28:56', NULL, 31),
	(16, 'aefe9d98-f602-4f09-aaea-55bf8130c2ea', '2021-04-19 14:45:12', '2021-04-20 14:45:12', NULL, 32),
	(17, '2b7fac2a-f604-4de7-a22c-f18a1886f649', '2021-04-19 15:11:50', '2021-04-20 15:11:50', '2021-04-19 15:12:34', 33),
	(18, '8d552579-688d-4d9c-b813-f4cee5af099c', '2021-04-19 15:15:56', '2021-04-19 16:15:56', '2021-04-19 15:19:57', 33),
	(19, '5504238e-d5e7-4149-8529-dc4e423bc3a5', '2021-04-19 15:39:51', '2021-04-19 16:39:51', '2021-04-19 16:11:52', 33),
	(20, 'fbb9f752-a5c8-4572-95a1-40200499d258', '2021-04-19 16:15:13', '2021-04-19 17:15:13', '2021-04-19 16:15:56', 33),
	(21, '35611a8c-765e-40c5-8a17-234eb19612c0', '2021-04-19 16:21:46', '2021-04-19 17:21:46', '2021-04-19 16:22:35', 33);
/*!40000 ALTER TABLE `confirmation_tokens` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.restart_password_requests
DROP TABLE IF EXISTS `restart_password_requests`;
CREATE TABLE IF NOT EXISTS `restart_password_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date_requested` datetime NOT NULL,
  `date_restarted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `restart_password_requests_users_id_fk` (`user_id`),
  CONSTRAINT `restart_password_requests_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.restart_password_requests: ~4 rows (приблизително)
/*!40000 ALTER TABLE `restart_password_requests` DISABLE KEYS */;
INSERT INTO `restart_password_requests` (`id`, `user_id`, `date_requested`, `date_restarted`) VALUES
	(7, 31, '2021-04-17 18:15:45', NULL),
	(8, 1, '2021-04-18 18:30:49', NULL),
	(9, 5, '2021-04-18 18:38:27', '2021-04-18 18:38:30'),
	(10, 33, '2021-04-19 15:18:59', '2021-04-19 16:22:35');
/*!40000 ALTER TABLE `restart_password_requests` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_role_uindex` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.roles: ~3 rows (приблизително)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `role`) VALUES
	(3, 'Admin'),
	(1, 'Customer'),
	(2, 'Employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.service_orders
DROP TABLE IF EXISTS `service_orders`;
CREATE TABLE IF NOT EXISTS `service_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `repairs_shop_services_id_fk` (`service_id`),
  KEY `service_orders_visits_id_fk` (`visit_id`),
  CONSTRAINT `repairs_shop_services_id_fk` FOREIGN KEY (`service_id`) REFERENCES `shop_services` (`id`),
  CONSTRAINT `service_orders_visits_id_fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.service_orders: ~3 rows (приблизително)
/*!40000 ALTER TABLE `service_orders` DISABLE KEYS */;
INSERT INTO `service_orders` (`id`, `service_id`, `visit_id`, `end_date`) VALUES
	(3, 1, 6, NULL),
	(4, 4, 6, '2021-04-19 00:00:00'),
	(5, 2, 7, '2021-05-01 00:00:00');
/*!40000 ALTER TABLE `service_orders` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.shop_services
DROP TABLE IF EXISTS `shop_services`;
CREATE TABLE IF NOT EXISTS `shop_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` float(25,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.shop_services: ~5 rows (приблизително)
/*!40000 ALTER TABLE `shop_services` DISABLE KEYS */;
INSERT INTO `shop_services` (`id`, `name`, `price`) VALUES
	(1, 'Oil Change', 50),
	(2, 'Engine Fix', 100),
	(3, 'Winshield Replacement', 150),
	(4, 'Tires Change', 20),
	(5, 'Brake Fluid', 1200);
/*!40000 ALTER TABLE `shop_services` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_uindex` (`email`),
  UNIQUE KEY `user_phone_uindex` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.users: ~25 rows (приблизително)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `last_name`, `phone`, `email`, `password`, `enabled`) VALUES
	(1, 'Victor', 'Valtchev', '0886634419', 'v.v@gmail.com', 'vvv', 1),
	(2, 'Atanas', 'Doychev', '0882345127', 'a.d@gmail.com', '123456789', 1),
	(3, 'Stefan', 'Petrov', '0887623442', 'admin@gmail.com', '123456789', 1),
	(4, 'Ralica', 'Ivanova', '0899999999', 'boss@gmail.com', '123456789', 1),
	(5, 'Stoqn', 'Dimitrov', '0892345123', 'customer.employee@gmail.com', '1234567890', 1),
	(6, 'Rosen', 'Petrov', '0884444448', 'rosen.p@gmail.com', '123456789', 1),
	(13, 'Silviq', 'Raleva', '0884444445', 's.raleva@gmail.com', '123456789', 1),
	(16, 'Qvor', 'Kolev', '0884444666', 'qvor.kolev@gmail.com', '123456789', 1),
	(17, 'Petq', 'Petrova', '0884444777', 'petq.p.p@gmail.com', '123456789', 1),
	(18, 'Victor', 'Vitkov', '0884444444', 'vic.vit@gmail.com', '123456789', 1),
	(19, 'Ivan', 'Dimitrov', '0884444789', 'dimitrov@gmail.com', '123456789', 1),
	(20, 'Locan', 'Petrov', '0884444555', 'lozan@gmail.com', '123456789', 1),
	(21, 'Chudomir', 'Manov', '0884441000', 'chudomir@abv.bg', '123456789', 0),
	(22, 'Katq', 'Kirilova', '0884442222', 'kateto@abv.bg', '123456789', 0),
	(23, 'Roman', 'Georgiev', '0884443333', 'roman.g@abv.bg', '123456789', 0),
	(24, 'Hristo', 'Stoqnov', '0884441234', 'hristo.s@abv.bg', '123456789', 0),
	(25, 'Veronika', 'Milanova', '0884449999', 'v.milanova@abv.bg', '123456789', 0),
	(26, 'Atanas', 'Atanasov', '0884440000', 'a.atanasov@abv.bg', '123456789', 0),
	(27, 'Rumen', 'Radev', '0884444567', 'r.radev@abv.bg', '123456789', 1),
	(28, 'Momchil', 'Genov', '0882220987', 'momchil.g@abv.bg', '123456789', 1),
	(29, 'Liliq', 'Koleva', '0880000023', 'liliq.koleva@gmail.com', '123456789', 1),
	(30, 'Rosen', 'Vanchev', '0000021333', 'Rosen.van@gmail.com', '123456789', 0),
	(31, 'Rangel', 'Todorov', '0665432444', 'rangel.t@gmail.com', '123456789', 0),
	(32, 'Ivelin', 'Rachkov', '0885246230', 'rachkov.i@gmail.com', 'c3e3eca7-7868-40ad-9e4f-3c4af54bfa95', 0),
	(33, 'Stoqnka', 'Petrova', '0554327888', 'smart.garage.first.customer@gmail.com', 'secondpasschange', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_roles_users_id_fk` (`user_id`),
  KEY `user_roles_roles_id_fk` (`role_id`),
  CONSTRAINT `user_roles_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `user_roles_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.user_roles: ~26 rows (приблизително)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`id`, `user_id`, `role_id`) VALUES
	(1, 1, 1),
	(2, 2, 2),
	(3, 3, 3),
	(4, 4, 2),
	(5, 4, 3),
	(6, 5, 1),
	(7, 5, 2),
	(9, 13, 1),
	(19, 16, 1),
	(20, 17, 1),
	(21, 18, 1),
	(22, 19, 1),
	(23, 20, 1),
	(24, 21, 1),
	(25, 22, 1),
	(26, 23, 1),
	(27, 24, 1),
	(28, 25, 1),
	(29, 26, 1),
	(30, 27, 1),
	(31, 28, 1),
	(32, 29, 1),
	(33, 30, 1),
	(34, 31, 1),
	(35, 32, 1),
	(36, 33, 1);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.vehicles
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `license_plate` varchar(8) NOT NULL,
  `vin` varchar(17) NOT NULL,
  `year` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vehicles_vin_uindex` (`vin`),
  UNIQUE KEY `plate_vin_combo` (`license_plate`,`vin`),
  KEY `vehicles_users_user_id_fk` (`owner_id`),
  KEY `vehicles_car_models_id_fk` (`model_id`),
  CONSTRAINT `vehicles_car_models_id_fk` FOREIGN KEY (`model_id`) REFERENCES `car_models` (`id`),
  CONSTRAINT `vehicles_users_user_id_fk` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.vehicles: ~15 rows (приблизително)
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`id`, `license_plate`, `vin`, `year`, `owner_id`, `model_id`) VALUES
	(1, 'B0707PP', '12345678912345678', 2000, 1, 1),
	(3, 'B1717PP', '12345678912340002', 2000, 1, 1),
	(4, 'B1718BP', '12345678900000000', 2020, 1, 1),
	(5, 'ca1234kk', '12345678912345666', 2000, 1, 1),
	(8, 'ca1234kk', '12345678912345667', 2000, 1, 1),
	(9, 'ca1234aa', '12345678912345888', 2000, 1, 1),
	(10, 'ca1234aa', '12345678912345333', 2000, 1, 1),
	(11, 'ca1234aa', '12345678912345999', 2000, 1, 1),
	(12, 'ca1234aa', '12345678912345111', 2000, 1, 1),
	(13, 'ca1234aa', '12345678912345969', 2000, 1, 1),
	(14, 'ca1234aa', '12345678912345123', 2000, 1, 1),
	(15, 'c1234ca', '12345678912345987', 2000, 18, 1),
	(16, 'ca3223kk', '12345673332345888', 1999, 29, 1),
	(17, 'ca3333cc', '12345676662345888', 2000, 29, 1),
	(18, 'ca9876kk', '88845678912345999', 2010, 32, 3);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- Дъмп структура за таблица smartgarage.visits
DROP TABLE IF EXISTS `visits`;
CREATE TABLE IF NOT EXISTS `visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `visits_vehicles_id_fk` (`vehicle_id`),
  CONSTRAINT `visits_vehicles_id_fk` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица smartgarage.visits: ~5 rows (приблизително)
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` (`id`, `vehicle_id`, `start_date`, `end_date`) VALUES
	(3, 1, '2021-04-09 13:06:11', '2021-04-10 13:06:59'),
	(4, 3, '2021-02-09 13:07:32', '2021-02-11 13:07:38'),
	(5, 15, '2021-04-04 21:02:15', '2021-04-10 21:02:55'),
	(6, 1, '2021-04-19 13:50:37', NULL),
	(7, 18, '2021-04-19 14:59:38', NULL);
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
