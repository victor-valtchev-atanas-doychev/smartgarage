# SmartGarage

Smart Garage is an 'auto repair shop' application for managing day-to-day jobs.
It supports public, user and work profiles with different functionality and access control.


Links:

Trello: https://trello.com/b/ZuX4HrOd
Trello: https://trello.com/invite/b/ZuX4HrOd/a8a2133939b9244259a438b676b32861/smartgarage

Swagger, API-documentation: https://app.swaggerhub.com/apis/SmartGarageVV/api-documentation/1.0



In order to run the application you need to follow these steps:

1. Clone the repository.
2. Open with IntelliJ
3. Run build.gradle(if it doesn't run automatically)
4. Execute the scripts provided in the 'database' folder.
5. Run the application from the DemoApplication class.

Images of the database relations are located in DataRelationsImages folder.

This is a RESTful, dynamic web application Build with: Java, JavaScript, Spring Boot,
MVC, REST, Gradle, Thymeleaf, Hibernate, MariaDB, HTML, CSS, jQuery, JUnit.